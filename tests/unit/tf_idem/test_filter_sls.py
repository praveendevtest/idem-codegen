import json


def test_filter_sls(hub):
    run_name = hub.OPT.idem_codegen.run_name
    expected_filtered_sls_data_path = (
        f"{hub.test.idem_codegen.current_path}/files/filtered_sls_data.json"
    )
    expected_tf_resource_map_path = (
        f"{hub.test.idem_codegen.current_path}/files/tf_resource_map.json"
    )

    with open(expected_filtered_sls_data_path) as file:
        data = file.read()
        expected_filtered_sls_data = json.loads(data)
    with open(expected_tf_resource_map_path) as file:
        data = file.read()
        expected_tf_resource_map = json.loads(data)

    hub.idem_codegen.compiler.init.compile(run_name)

    tf_data = hub.tf_idem.RUNS["TF_STATE_DATA"]
    sls_data = hub.idem_codegen.tool.utils.parse_sls_data(
        hub.OPT.idem_codegen.idem_describe_path
    )
    (
        tf_resource_type_name_and_resource_map,
        filtered_sls_data,
    ) = hub.tf_idem.exec.compiler.compile.compare_and_filter_sls(
        tf_data["resources"], sls_data
    )

    assert tf_resource_type_name_and_resource_map and filtered_sls_data
    assert expected_tf_resource_map == tf_resource_type_name_and_resource_map
    assert expected_filtered_sls_data == filtered_sls_data
