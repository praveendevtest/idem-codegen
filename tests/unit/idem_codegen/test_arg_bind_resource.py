from collections import ChainMap

import pytest
import yaml


@pytest.mark.asyncio
def test_arg_bind_resource(hub):
    sls_data = yaml.safe_load(
        open(f"{hub.test.idem_codegen.current_path}/files/arg_bind.sls")
    )

    idem_resource_id_map = hub.idem_codegen.exec.generator.generate.resource_id_map(
        sls_data
    )

    arg_binded_sls_data = hub.idem_codegen.generator.argument_binder.default.arg_bind(
        sls_data, idem_resource_id_map
    )
    resource_attributes = list(
        arg_binded_sls_data[
            "AWSServiceRoleForCloudFrontLogger-arn:aws:iam::aws:policy/aws-service-role/AWSCloudFrontLogger"
        ].values()
    )[0]
    resource_map = dict(ChainMap(*resource_attributes))
    assert (
        "${aws.iam.role:AWSServiceRoleForCloudFrontLogger:resource_id}"
        == resource_map.get("role_name")
    )

    resource_attributes = list(arg_binded_sls_data["idem-test-us-2"].values())[0]
    resource_map = dict(ChainMap(*resource_attributes))
    assert (
        "${aws.iam.role:idem-test-us-2-terraform-eks-cluster:arn}"
        == resource_map.get("role_arn")
    )

    # checking if we are able to change inside a dictionary
    assert (
        "${aws.ec2.security_group:sg-043b20ebd44e776bb:resource_id}"
        == resource_map.get("resources_vpc_config").get("securityGroupIds")[0]
    )
    assert (
        "${aws.ec2.security_group:sg-0026c929f9ca07ddd:resource_id}"
        == resource_map.get("resources_vpc_config").get("clusterSecurityGroupId")
    )

    # checking if we are able to change in a list inside a dictionary
    assert "${aws.ec2.subnet:subnet-0ae31e9f080fdd458:resource_id}" in resource_map.get(
        "resources_vpc_config"
    ).get("subnetIds")
    assert "${aws.ec2.subnet:subnet-0a2e12d271208342f:resource_id}" in resource_map.get(
        "resources_vpc_config"
    ).get("subnetIds")

    resource_attributes = list(
        arg_binded_sls_data[
            "eks-idem-test-us-2-ccs-idem-test-admin-terraform-20211122095108358600000003"
        ].values()
    )[0]
    resource_map = dict(ChainMap(*resource_attributes))
    # checking if we are able to change in a json policy document
    assert (
        '{"Statement": [{"Action": "eks:DescribeCluster", "Effect": "Allow", "Resource": "${aws.eks.cluster:idem-test-us-2:arn}"}], "Version": "2012-10-17"}'
        == resource_map.get("policy_document")
    )
