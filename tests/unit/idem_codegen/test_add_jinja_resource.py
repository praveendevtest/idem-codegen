import io
import json

import pytest
import yaml


@pytest.mark.asyncio
def test_add_jinja_resource(hub):
    jinja_dump_data = io.StringIO()
    sls_data = yaml.safe_load(
        open(f"{hub.test.idem_codegen.current_path}/files/arg_and_parameterized.sls")
    )
    sls_original_data = yaml.safe_load(
        open(f"{hub.test.idem_codegen.current_path}/files/sls_original_data.sls")
    )

    hub.tf_idem.RUNS["TF_RESOURCE_MAP"] = json.load(
        open(f"{hub.test.idem_codegen.current_path}/files/terraform_resource_map.json")
    )
    jinja_dump_data = hub.idem_codegen.generator.jinja.init.execute(
        "tf_idem", sls_data, sls_original_data
    )
    assert "{% for k in range(3) %}" in jinja_dump_data.getvalue()
    assert (
        "The attribute 'subnet_ids' has resolved value of 'data' state"
        in jinja_dump_data.getvalue()
    )
