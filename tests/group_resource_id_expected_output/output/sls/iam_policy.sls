arn:aws:iam::123456789012:policy/ALBIngressControllerIAMPolicy:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["acm:DescribeCertificate", "acm:ListCertificates",
      "acm:GetCertificate"], "Effect": "Allow", "Resource": "*"}, {"Action": ["ec2:AuthorizeSecurityGroupIngress",
      "ec2:CreateSecurityGroup", "ec2:CreateTags", "ec2:DeleteTags", "ec2:DeleteSecurityGroup",
      "ec2:DescribeAccountAttributes", "ec2:DescribeAddresses", "ec2:DescribeInstances",
      "ec2:DescribeInstanceStatus", "ec2:DescribeInternetGateways", "ec2:DescribeNetworkInterfaces",
      "ec2:DescribeSecurityGroups", "ec2:DescribeSubnets", "ec2:DescribeTags", "ec2:DescribeVpcs",
      "ec2:ModifyInstanceAttribute", "ec2:ModifyNetworkInterfaceAttribute", "ec2:RevokeSecurityGroupIngress"],
      "Effect": "Allow", "Resource": "*"}, {"Action": ["elasticloadbalancing:AddListenerCertificates",
      "elasticloadbalancing:AddTags", "elasticloadbalancing:CreateListener", "elasticloadbalancing:CreateLoadBalancer",
      "elasticloadbalancing:CreateRule", "elasticloadbalancing:CreateTargetGroup",
      "elasticloadbalancing:DeleteListener", "elasticloadbalancing:DeleteLoadBalancer",
      "elasticloadbalancing:DeleteRule", "elasticloadbalancing:DeleteTargetGroup",
      "elasticloadbalancing:DeregisterTargets", "elasticloadbalancing:DescribeListenerCertificates",
      "elasticloadbalancing:DescribeListeners", "elasticloadbalancing:DescribeLoadBalancers",
      "elasticloadbalancing:DescribeLoadBalancerAttributes", "elasticloadbalancing:DescribeRules",
      "elasticloadbalancing:DescribeSSLPolicies", "elasticloadbalancing:DescribeTags",
      "elasticloadbalancing:DescribeTargetGroups", "elasticloadbalancing:DescribeTargetGroupAttributes",
      "elasticloadbalancing:DescribeTargetHealth", "elasticloadbalancing:ModifyListener",
      "elasticloadbalancing:ModifyLoadBalancerAttributes", "elasticloadbalancing:ModifyRule",
      "elasticloadbalancing:ModifyTargetGroup", "elasticloadbalancing:ModifyTargetGroupAttributes",
      "elasticloadbalancing:RegisterTargets", "elasticloadbalancing:RemoveListenerCertificates",
      "elasticloadbalancing:RemoveTags", "elasticloadbalancing:SetIpAddressType",
      "elasticloadbalancing:SetSecurityGroups", "elasticloadbalancing:SetSubnets",
      "elasticloadbalancing:SetWebACL"], "Effect": "Allow", "Resource": "*"}, {"Action":
      ["iam:CreateServiceLinkedRole", "iam:GetServerCertificate", "iam:ListServerCertificates"],
      "Effect": "Allow", "Resource": "*"}, {"Action": ["cognito-idp:DescribeUserPoolClient"],
      "Effect": "Allow", "Resource": "*"}, {"Action": ["waf-regional:GetWebACLForResource",
      "waf-regional:GetWebACL", "waf-regional:AssociateWebACL", "waf-regional:DisassociateWebACL"],
      "Effect": "Allow", "Resource": "*"}, {"Action": ["tag:GetResources", "tag:TagResources"],
      "Effect": "Allow", "Resource": "*"}, {"Action": ["waf:GetWebACL"], "Effect":
      "Allow", "Resource": "*"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: ALBIngressControllerIAMPolicy
  - resource_id: arn:aws:iam::123456789012:policy/ALBIngressControllerIAMPolicy
  - id: ANPAX2FJ77DC6VVN4M7W5
  - path: /


arn:aws:iam::123456789012:policy/AWSLoadBalancerControllerAdditionalIAMPolicy:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:CreateTags", "ec2:DeleteTags"],
      "Condition": {"Null": {"aws:ResourceTag/ingress.k8s.aws/cluster": "false"}},
      "Effect": "Allow", "Resource": "arn:aws:ec2:*:*:security-group/*"}, {"Action":
      ["elasticloadbalancing:AddTags", "elasticloadbalancing:RemoveTags", "elasticloadbalancing:DeleteTargetGroup"],
      "Condition": {"Null": {"aws:ResourceTag/ingress.k8s.aws/cluster": "false"}},
      "Effect": "Allow", "Resource": ["arn:aws:elasticloadbalancing:*:*:targetgroup/*/*",
      "arn:aws:elasticloadbalancing:*:*:loadbalancer/net/*/*", "arn:aws:elasticloadbalancing:*:*:loadbalancer/app/*/*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLoadBalancerControllerAdditionalIAMPolicy
  - resource_id: arn:aws:iam::123456789012:policy/AWSLoadBalancerControllerAdditionalIAMPolicy
  - id: ANPAX2FJ77DCRUTGRW5FW
  - path: /


arn:aws:iam::123456789012:policy/AWSLoadBalancerControllerIAMPolicy:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "iam:CreateServiceLinkedRole", "Condition":
      {"StringEquals": {"iam:AWSServiceName": "elasticloadbalancing.amazonaws.com"}},
      "Effect": "Allow", "Resource": "*"}, {"Action": ["ec2:DescribeAccountAttributes",
      "ec2:DescribeAddresses", "ec2:DescribeAvailabilityZones", "ec2:DescribeInternetGateways",
      "ec2:DescribeVpcs", "ec2:DescribeVpcPeeringConnections", "ec2:DescribeSubnets",
      "ec2:DescribeSecurityGroups", "ec2:DescribeInstances", "ec2:DescribeNetworkInterfaces",
      "ec2:DescribeTags", "ec2:GetCoipPoolUsage", "ec2:DescribeCoipPools", "elasticloadbalancing:DescribeLoadBalancers",
      "elasticloadbalancing:DescribeLoadBalancerAttributes", "elasticloadbalancing:DescribeListeners",
      "elasticloadbalancing:DescribeListenerCertificates", "elasticloadbalancing:DescribeSSLPolicies",
      "elasticloadbalancing:DescribeRules", "elasticloadbalancing:DescribeTargetGroups",
      "elasticloadbalancing:DescribeTargetGroupAttributes", "elasticloadbalancing:DescribeTargetHealth",
      "elasticloadbalancing:DescribeTags"], "Effect": "Allow", "Resource": "*"}, {"Action":
      ["cognito-idp:DescribeUserPoolClient", "acm:ListCertificates", "acm:DescribeCertificate",
      "iam:ListServerCertificates", "iam:GetServerCertificate", "waf-regional:GetWebACL",
      "waf-regional:GetWebACLForResource", "waf-regional:AssociateWebACL", "waf-regional:DisassociateWebACL",
      "wafv2:GetWebACL", "wafv2:GetWebACLForResource", "wafv2:AssociateWebACL", "wafv2:DisassociateWebACL",
      "shield:GetSubscriptionState", "shield:DescribeProtection", "shield:CreateProtection",
      "shield:DeleteProtection"], "Effect": "Allow", "Resource": "*"}, {"Action":
      ["ec2:AuthorizeSecurityGroupIngress", "ec2:RevokeSecurityGroupIngress"], "Effect":
      "Allow", "Resource": "*"}, {"Action": ["ec2:CreateSecurityGroup"], "Effect":
      "Allow", "Resource": "*"}, {"Action": ["ec2:CreateTags"], "Condition": {"Null":
      {"aws:RequestTag/elbv2.k8s.aws/cluster": "false"}, "StringEquals": {"ec2:CreateAction":
      "CreateSecurityGroup"}}, "Effect": "Allow", "Resource": "arn:aws:ec2:*:*:security-group/*"},
      {"Action": ["ec2:CreateTags", "ec2:DeleteTags"], "Condition": {"Null": {"aws:RequestTag/elbv2.k8s.aws/cluster":
      "true", "aws:ResourceTag/elbv2.k8s.aws/cluster": "false"}}, "Effect": "Allow",
      "Resource": "arn:aws:ec2:*:*:security-group/*"}, {"Action": ["ec2:AuthorizeSecurityGroupIngress",
      "ec2:RevokeSecurityGroupIngress", "ec2:DeleteSecurityGroup"], "Condition": {"Null":
      {"aws:ResourceTag/elbv2.k8s.aws/cluster": "false"}}, "Effect": "Allow", "Resource":
      "*"}, {"Action": ["elasticloadbalancing:CreateLoadBalancer", "elasticloadbalancing:CreateTargetGroup"],
      "Condition": {"Null": {"aws:RequestTag/elbv2.k8s.aws/cluster": "false"}}, "Effect":
      "Allow", "Resource": "*"}, {"Action": ["elasticloadbalancing:CreateListener",
      "elasticloadbalancing:DeleteListener", "elasticloadbalancing:CreateRule", "elasticloadbalancing:DeleteRule"],
      "Effect": "Allow", "Resource": "*"}, {"Action": ["elasticloadbalancing:AddTags",
      "elasticloadbalancing:RemoveTags"], "Condition": {"Null": {"aws:RequestTag/elbv2.k8s.aws/cluster":
      "true", "aws:ResourceTag/elbv2.k8s.aws/cluster": "false"}}, "Effect": "Allow",
      "Resource": ["arn:aws:elasticloadbalancing:*:*:targetgroup/*/*", "arn:aws:elasticloadbalancing:*:*:loadbalancer/net/*/*",
      "arn:aws:elasticloadbalancing:*:*:loadbalancer/app/*/*"]}, {"Action": ["elasticloadbalancing:AddTags",
      "elasticloadbalancing:RemoveTags"], "Effect": "Allow", "Resource": ["arn:aws:elasticloadbalancing:*:*:listener/net/*/*/*",
      "arn:aws:elasticloadbalancing:*:*:listener/app/*/*/*", "arn:aws:elasticloadbalancing:*:*:listener-rule/net/*/*/*",
      "arn:aws:elasticloadbalancing:*:*:listener-rule/app/*/*/*"]}, {"Action": ["elasticloadbalancing:ModifyLoadBalancerAttributes",
      "elasticloadbalancing:SetIpAddressType", "elasticloadbalancing:SetSecurityGroups",
      "elasticloadbalancing:SetSubnets", "elasticloadbalancing:DeleteLoadBalancer",
      "elasticloadbalancing:ModifyTargetGroup", "elasticloadbalancing:ModifyTargetGroupAttributes",
      "elasticloadbalancing:DeleteTargetGroup"], "Condition": {"Null": {"aws:ResourceTag/elbv2.k8s.aws/cluster":
      "false"}}, "Effect": "Allow", "Resource": "*"}, {"Action": ["elasticloadbalancing:RegisterTargets",
      "elasticloadbalancing:DeregisterTargets"], "Effect": "Allow", "Resource": "arn:aws:elasticloadbalancing:*:*:targetgroup/*/*"},
      {"Action": ["elasticloadbalancing:SetWebAcl", "elasticloadbalancing:ModifyListener",
      "elasticloadbalancing:AddListenerCertificates", "elasticloadbalancing:RemoveListenerCertificates",
      "elasticloadbalancing:ModifyRule"], "Effect": "Allow", "Resource": "*"}], "Version":
      "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLoadBalancerControllerIAMPolicy
  - resource_id: arn:aws:iam::123456789012:policy/AWSLoadBalancerControllerIAMPolicy
  - id: ANPAX2FJ77DC5VVD5KYUI
  - path: /


arn:aws:iam::123456789012:policy/AllowEC2ElasticIPReadWrite:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:ReleaseAddress", "ec2:DisassociateAddress",
      "ec2:DescribeAddresses", "ec2:DescribeInstances", "ec2:DescribeNetworkInterfaces",
      "ec2:AssociateAddress", "ec2:AllocateAddress"], "Effect": "Allow", "Resource":
      "*", "Sid": "VisualEditor0"}], "Version": "2012-10-17"}'
  - default_version_id: v6
  - tags: []
  - name: AllowEC2ElasticIPReadWrite
  - resource_id: arn:aws:iam::123456789012:policy/AllowEC2ElasticIPReadWrite
  - id: ANPAX2FJ77DCQUYBXL54N
  - path: /


arn:aws:iam::123456789012:policy/AmazonFori18ntest+=,.@-_:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["autoscaling:Describe*", "cloudwatch:Describe*",
      "cloudwatch:Get*", "cloudwatch:List*", "ec2:Describe*", "elasticloadbalancing:Describe*",
      "s3:Get*", "s3:List*"], "Effect": "Allow", "Resource": ["*"], "Sid": "1"}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AmazonFori18ntest+=,.@-_
  - resource_id: arn:aws:iam::123456789012:policy/AmazonFori18ntest+=,.@-_
  - id: ANPAIPPXXIXAP67XMGXYW
  - path: /


arn:aws:iam::123456789012:policy/Amazonxyz_EBS_CSI_Driver_Policy:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:CreateSnapshot", "ec2:AttachVolume",
      "ec2:DetachVolume", "ec2:ModifyVolume", "ec2:DescribeAvailabilityZones", "ec2:DescribeInstances",
      "ec2:DescribeSnapshots", "ec2:DescribeTags", "ec2:DescribeVolumes", "ec2:DescribeVolumesModifications"],
      "Effect": "Allow", "Resource": "*"}, {"Action": ["ec2:CreateTags"], "Condition":
      {"StringEquals": {"ec2:CreateAction": ["CreateVolume", "CreateSnapshot"]}},
      "Effect": "Allow", "Resource": ["arn:aws:ec2:*:*:volume/*", "arn:aws:ec2:*:*:snapshot/*"]},
      {"Action": ["ec2:DeleteTags"], "Effect": "Allow", "Resource": ["arn:aws:ec2:*:*:volume/*",
      "arn:aws:ec2:*:*:snapshot/*"]}, {"Action": ["ec2:CreateVolume"], "Condition":
      {"StringLike": {"aws:RequestTag/ebs.csi.aws.com/cluster": "true"}}, "Effect":
      "Allow", "Resource": "*"}, {"Action": ["ec2:CreateVolume"], "Condition": {"StringLike":
      {"aws:RequestTag/CSIVolumeName": "*"}}, "Effect": "Allow", "Resource": "*"},
      {"Action": ["ec2:CreateVolume"], "Condition": {"StringLike": {"aws:RequestTag/kubernetes.io/cluster/*":
      "owned"}}, "Effect": "Allow", "Resource": "*"}, {"Action": ["ec2:DeleteVolume"],
      "Condition": {"StringLike": {"ec2:ResourceTag/ebs.csi.aws.com/cluster": "true"}},
      "Effect": "Allow", "Resource": "*"}, {"Action": ["ec2:DeleteVolume"], "Condition":
      {"StringLike": {"ec2:ResourceTag/CSIVolumeName": "*"}}, "Effect": "Allow", "Resource":
      "*"}, {"Action": ["ec2:DeleteVolume"], "Condition": {"StringLike": {"ec2:ResourceTag/kubernetes.io/cluster/*":
      "owned"}}, "Effect": "Allow", "Resource": "*"}, {"Action": ["ec2:DeleteSnapshot"],
      "Condition": {"StringLike": {"ec2:ResourceTag/CSIVolumeSnapshotName": "*"}},
      "Effect": "Allow", "Resource": "*"}, {"Action": ["ec2:DeleteSnapshot"], "Condition":
      {"StringLike": {"ec2:ResourceTag/ebs.csi.aws.com/cluster": "true"}}, "Effect":
      "Allow", "Resource": "*"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: Amazonxyz_EBS_CSI_Driver_Policy
  - resource_id: arn:aws:iam::123456789012:policy/Amazonxyz_EBS_CSI_Driver_Policy
  - id: ANPAX2FJ77DC4MFI3OXEL
  - path: /


arn:aws:iam::123456789012:policy/AzureEventQueuePolicy:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["sqs:SendMessage", "sqs:ReceiveMessage"],
      "Effect": "Allow", "Resource": ["arn:aws:sqs:us-east-2:123456789012:azuredemoqueue.fifo"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v2
  - tags: []
  - name: AzureEventQueuePolicy
  - resource_id: arn:aws:iam::123456789012:policy/AzureEventQueuePolicy
  - id: ANPAIEJ6247JCH4E4QZBI
  - path: /


arn:aws:iam::123456789012:policy/CHTPolicy_Borathon:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["autoscaling:Describe*", "aws-portal:ViewBilling",
      "aws-portal:ViewUsage", "cloudformation:ListStacks", "cloudformation:ListStackResources",
      "cloudformation:DescribeStacks", "cloudformation:DescribeStackEvents", "cloudformation:DescribeStackResources",
      "cloudformation:GetTemplate", "cloudfront:Get*", "cloudfront:List*", "cloudtrail:DescribeTrails",
      "cloudtrail:GetEventSelectors", "cloudtrail:ListTags", "cloudwatch:Describe*",
      "cloudwatch:Get*", "cloudwatch:List*", "config:Get*", "config:Describe*", "config:Deliver*",
      "config:List*", "cur:Describe*", "dms:Describe*", "dms:List*", "dynamodb:DescribeTable",
      "dynamodb:List*", "ec2:Describe*", "ec2:GetReservedInstancesExchangeQuote",
      "ecs:List*", "ecs:Describe*", "elasticache:Describe*", "elasticache:ListTagsForResource",
      "elasticbeanstalk:Check*", "elasticbeanstalk:Describe*", "elasticbeanstalk:List*",
      "elasticbeanstalk:RequestEnvironmentInfo", "elasticbeanstalk:RetrieveEnvironmentInfo",
      "elasticfilesystem:Describe*", "elasticloadbalancing:Describe*", "elasticmapreduce:Describe*",
      "elasticmapreduce:List*", "es:List*", "es:Describe*", "firehose:ListDeliveryStreams",
      "firehose:DescribeDeliveryStream", "iam:List*", "iam:Get*", "iam:GenerateCredentialReport",
      "kinesis:Describe*", "kinesis:List*", "kms:DescribeKey", "kms:GetKeyRotationStatus",
      "kms:ListKeys", "lambda:List*", "logs:Describe*", "redshift:Describe*", "route53:Get*",
      "route53:List*", "rds:Describe*", "rds:ListTagsForResource", "s3:GetBucketAcl",
      "s3:GetBucketLocation", "s3:GetBucketLogging", "s3:GetBucketPolicy", "s3:GetBucketTagging",
      "s3:GetBucketVersioning", "s3:GetBucketWebsite", "s3:List*", "sagemaker:Describe*",
      "sagemaker:List*", "sdb:GetAttributes", "sdb:List*", "ses:Get*", "ses:List*",
      "sns:Get*", "sns:List*", "sqs:GetQueueAttributes", "sqs:ListQueues", "storagegateway:List*",
      "storagegateway:Describe*", "workspaces:Describe*"], "Effect": "Allow", "Resource":
      "*"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: CHTPolicy_Borathon
  - resource_id: arn:aws:iam::123456789012:policy/CHTPolicy_Borathon
  - id: ANPAX2FJ77DC2B7YAZMZ6
  - path: /


arn:aws:iam::123456789012:policy/DescribeImagesPolicy:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:DescribeImages"], "Effect":
      "Allow", "Resource": "*"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: DescribeImagesPolicy
  - resource_id: arn:aws:iam::123456789012:policy/DescribeImagesPolicy
  - id: ANPAX2FJ77DC6FMEIRL7I
  - path: /


arn:aws:iam::123456789012:policy/EC2ListResources:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:DescribeInstances", "ec2:DescribeAggregateIdFormat",
      "ec2:DescribeSnapshots", "ec2:DescribePlacementGroups", "ec2:DescribeHostReservationOfferings",
      "ec2:DescribeInternetGateways", "ec2:DescribeVolumeStatus", "ec2:DescribeSpotDatafeedSubscription",
      "ec2:DescribeVolumes", "ec2:DescribeFpgaImageAttribute", "ec2:DescribeExportTasks",
      "ec2:DescribeAccountAttributes", "ec2:DescribeNetworkInterfacePermissions",
      "ec2:DescribeReservedInstances", "ec2:DescribeKeyPairs", "ec2:DescribeNetworkAcls",
      "ec2:DescribeRouteTables", "ec2:DescribeReservedInstancesListings", "ec2:DescribeEgressOnlyInternetGateways",
      "ec2:DescribeSpotFleetRequestHistory", "ec2:DescribeLaunchTemplates", "ec2:DescribeVpcClassicLinkDnsSupport",
      "ec2:DescribeSnapshotAttribute", "ec2:DescribeVpcPeeringConnections", "ec2:DescribeReservedInstancesOfferings",
      "ec2:DescribeIdFormat", "ec2:DescribeFleetInstances", "ec2:DescribeVpcEndpointServiceConfigurations",
      "ec2:DescribePrefixLists", "ec2:DescribeVolumeAttribute", "ec2:DescribeInstanceCreditSpecifications",
      "ec2:DescribeVpcClassicLink", "ec2:DescribeImportSnapshotTasks", "ec2:DescribeVpcEndpointServicePermissions",
      "ec2:DescribeImageAttribute", "ec2:DescribeFleets", "ec2:DescribeVpcEndpoints",
      "ec2:DescribeReservedInstancesModifications", "ec2:DescribeSubnets", "ec2:DescribeVpnGateways",
      "ec2:DescribeMovingAddresses", "ec2:DescribeFleetHistory", "ec2:DescribePrincipalIdFormat",
      "ec2:DescribeAddresses", "ec2:DescribeInstanceAttribute", "ec2:DescribeRegions",
      "ec2:DescribeFlowLogs", "ec2:DescribeDhcpOptions", "ec2:DescribeVpcEndpointServices",
      "ec2:DescribeSpotInstanceRequests", "ec2:DescribeVpcAttribute", "ec2:DescribeSpotPriceHistory",
      "ec2:DescribeNetworkInterfaces", "ec2:DescribeAvailabilityZones", "ec2:DescribeNetworkInterfaceAttribute",
      "ec2:DescribeVpcEndpointConnections", "ec2:DescribeInstanceStatus", "ec2:DescribeHostReservations",
      "ec2:DescribeIamInstanceProfileAssociations", "ec2:DescribeLaunchTemplateVersions",
      "ec2:DescribeBundleTasks", "ec2:DescribeIdentityIdFormat", "ec2:DescribeImportImageTasks",
      "ec2:DescribeClassicLinkInstances", "ec2:DescribeNatGateways", "ec2:DescribeCustomerGateways",
      "ec2:DescribeVpcEndpointConnectionNotifications", "ec2:DescribeSecurityGroups",
      "ec2:DescribeSpotFleetRequests", "ec2:DescribeHosts", "ec2:DescribeImages",
      "ec2:DescribeFpgaImages", "ec2:DescribeSpotFleetInstances", "ec2:DescribeSecurityGroupReferences",
      "ec2:DescribeVpcs", "ec2:DescribeConversionTasks", "ec2:DescribeStaleSecurityGroups"],
      "Effect": "Allow", "Resource": "*", "Sid": "VisualEditor0"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: EC2ListResources
  - resource_id: arn:aws:iam::123456789012:policy/EC2ListResources
  - id: ANPAILKUVUHIUSA3KUTQW
  - path: /


arn:aws:iam::123456789012:policy/KK=+WW_EES:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["autoscaling:Describe*", "cloudwatch:Describe*",
      "cloudwatch:Get*", "cloudwatch:List*", "ec2:Describe*", "elasticloadbalancing:Describe*",
      "s3:Get*", "s3:List*"], "Effect": "Allow", "Resource": ["*"], "Sid": "1"}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: KK=+WW_EES
  - resource_id: arn:aws:iam::123456789012:policy/KK=+WW_EES
  - id: ANPAI3PZT5EAIULAN2HGI
  - path: /


arn:aws:iam::123456789012:policy/apigateway-sqs-access-policy:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["sqs:SendMessage", "sqs:ReceiveMessage"],
      "Effect": "Allow", "Resource": ["arn:aws:sqs:us-east-2:123456789012:azuredemoqueue.fifo"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: apigateway-sqs-access-policy
  - resource_id: arn:aws:iam::123456789012:policy/apigateway-sqs-access-policy
  - id: ANPAJP6WUCIFEUQ2SLGUQ
  - path: /


arn:aws:iam::123456789012:policy/azure-events-forwarder-lambda-iam:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "lambda:*", "Effect": "Allow", "Resource":
      "*", "Sid": "VisualEditor0"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: azure-events-forwarder-lambda-iam
  - resource_id: arn:aws:iam::123456789012:policy/azure-events-forwarder-lambda-iam
  - id: ANPAJS2XE3NYTBWNWSEKC
  - path: /


arn:aws:iam::123456789012:policy/control-plane.tkg.cloud.abc.com:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["autoscaling:DescribeAutoScalingGroups",
      "autoscaling:DescribeLaunchConfigurations", "autoscaling:DescribeTags", "ec2:DescribeInstances",
      "ec2:DescribeImages", "ec2:DescribeRegions", "ec2:DescribeRouteTables", "ec2:DescribeSecurityGroups",
      "ec2:DescribeSubnets", "ec2:DescribeVolumes", "ec2:CreateSecurityGroup", "ec2:CreateTags",
      "ec2:CreateVolume", "ec2:ModifyInstanceAttribute", "ec2:ModifyVolume", "ec2:AttachVolume",
      "ec2:AuthorizeSecurityGroupIngress", "ec2:CreateRoute", "ec2:DeleteRoute", "ec2:DeleteSecurityGroup",
      "ec2:DeleteVolume", "ec2:DetachVolume", "ec2:RevokeSecurityGroupIngress", "ec2:DescribeVpcs",
      "elasticloadbalancing:AddTags", "elasticloadbalancing:AttachLoadBalancerToSubnets",
      "elasticloadbalancing:ApplySecurityGroupsToLoadBalancer", "elasticloadbalancing:CreateLoadBalancer",
      "elasticloadbalancing:CreateLoadBalancerPolicy", "elasticloadbalancing:CreateLoadBalancerListeners",
      "elasticloadbalancing:ConfigureHealthCheck", "elasticloadbalancing:DeleteLoadBalancer",
      "elasticloadbalancing:DeleteLoadBalancerListeners", "elasticloadbalancing:DescribeLoadBalancers",
      "elasticloadbalancing:DescribeLoadBalancerAttributes", "elasticloadbalancing:DetachLoadBalancerFromSubnets",
      "elasticloadbalancing:DeregisterInstancesFromLoadBalancer", "elasticloadbalancing:ModifyLoadBalancerAttributes",
      "elasticloadbalancing:RegisterInstancesWithLoadBalancer", "elasticloadbalancing:SetLoadBalancerPoliciesForBackendServer",
      "elasticloadbalancing:AddTags", "elasticloadbalancing:CreateListener", "elasticloadbalancing:CreateTargetGroup",
      "elasticloadbalancing:DeleteListener", "elasticloadbalancing:DeleteTargetGroup",
      "elasticloadbalancing:DescribeListeners", "elasticloadbalancing:DescribeLoadBalancerPolicies",
      "elasticloadbalancing:DescribeTargetGroups", "elasticloadbalancing:DescribeTargetHealth",
      "elasticloadbalancing:ModifyListener", "elasticloadbalancing:ModifyTargetGroup",
      "elasticloadbalancing:RegisterTargets", "elasticloadbalancing:SetLoadBalancerPoliciesOfListener",
      "iam:CreateServiceLinkedRole", "kms:DescribeKey"], "Effect": "Allow", "Resource":
      ["*"]}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: control-plane.tkg.cloud.abc.com
  - resource_id: arn:aws:iam::123456789012:policy/control-plane.tkg.cloud.abc.com
  - id: ANPAX2FJ77DC22ZNIPHUE
  - path: /


arn:aws:iam::123456789012:policy/control-plane.tmc.cloud.abc.com:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["autoscaling:DescribeAutoScalingGroups",
      "autoscaling:DescribeLaunchConfigurations", "autoscaling:DescribeTags", "ec2:DescribeInstances",
      "ec2:DescribeImages", "ec2:DescribeRegions", "ec2:DescribeRouteTables", "ec2:DescribeSecurityGroups",
      "ec2:DescribeSubnets", "ec2:DescribeVolumes", "ec2:CreateSecurityGroup", "ec2:CreateTags",
      "ec2:CreateVolume", "ec2:ModifyInstanceAttribute", "ec2:ModifyVolume", "ec2:AttachVolume",
      "ec2:AuthorizeSecurityGroupIngress", "ec2:CreateRoute", "ec2:DeleteRoute", "ec2:DeleteSecurityGroup",
      "ec2:DeleteVolume", "ec2:DetachVolume", "ec2:RevokeSecurityGroupIngress", "ec2:DescribeVpcs",
      "elasticloadbalancing:AddTags", "elasticloadbalancing:AttachLoadBalancerToSubnets",
      "elasticloadbalancing:ApplySecurityGroupsToLoadBalancer", "elasticloadbalancing:CreateLoadBalancer",
      "elasticloadbalancing:CreateLoadBalancerPolicy", "elasticloadbalancing:CreateLoadBalancerListeners",
      "elasticloadbalancing:ConfigureHealthCheck", "elasticloadbalancing:DeleteLoadBalancer",
      "elasticloadbalancing:DeleteLoadBalancerListeners", "elasticloadbalancing:DescribeLoadBalancers",
      "elasticloadbalancing:DescribeLoadBalancerAttributes", "elasticloadbalancing:DetachLoadBalancerFromSubnets",
      "elasticloadbalancing:DeregisterInstancesFromLoadBalancer", "elasticloadbalancing:ModifyLoadBalancerAttributes",
      "elasticloadbalancing:RegisterInstancesWithLoadBalancer", "elasticloadbalancing:SetLoadBalancerPoliciesForBackendServer",
      "elasticloadbalancing:AddTags", "elasticloadbalancing:CreateListener", "elasticloadbalancing:CreateTargetGroup",
      "elasticloadbalancing:DeleteListener", "elasticloadbalancing:DeleteTargetGroup",
      "elasticloadbalancing:DescribeListeners", "elasticloadbalancing:DescribeLoadBalancerPolicies",
      "elasticloadbalancing:DescribeTargetGroups", "elasticloadbalancing:DescribeTargetHealth",
      "elasticloadbalancing:ModifyListener", "elasticloadbalancing:ModifyTargetGroup",
      "elasticloadbalancing:RegisterTargets", "elasticloadbalancing:SetLoadBalancerPoliciesOfListener",
      "iam:CreateServiceLinkedRole", "kms:DescribeKey"], "Effect": "Allow", "Resource":
      ["*"]}, {"Action": ["secretsmanager:DeleteSecret", "secretsmanager:GetSecretValue"],
      "Effect": "Allow", "Resource": ["arn:aws:secretsmanager:*:*:secret:aws.cluster.x-k8s.io/*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: control-plane.tmc.cloud.abc.com
  - resource_id: arn:aws:iam::123456789012:policy/control-plane.tmc.cloud.abc.com
  - id: ANPAX2FJ77DC7IE5MMLRD
  - path: /


arn:aws:iam::123456789012:policy/controllers.tkg.cloud.abc.com:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:AllocateAddress", "ec2:AssociateRouteTable",
      "ec2:AttachInternetGateway", "ec2:AuthorizeSecurityGroupIngress", "ec2:CreateInternetGateway",
      "ec2:CreateNatGateway", "ec2:CreateRoute", "ec2:CreateRouteTable", "ec2:CreateSecurityGroup",
      "ec2:CreateSubnet", "ec2:CreateTags", "ec2:CreateVpc", "ec2:ModifyVpcAttribute",
      "ec2:DeleteInternetGateway", "ec2:DeleteNatGateway", "ec2:DeleteRouteTable",
      "ec2:DeleteSecurityGroup", "ec2:DeleteSubnet", "ec2:DeleteTags", "ec2:DeleteVpc",
      "ec2:DescribeAccountAttributes", "ec2:DescribeAddresses", "ec2:DescribeAvailabilityZones",
      "ec2:DescribeInstances", "ec2:DescribeInternetGateways", "ec2:DescribeImages",
      "ec2:DescribeNatGateways", "ec2:DescribeNetworkInterfaces", "ec2:DescribeNetworkInterfaceAttribute",
      "ec2:DescribeRouteTables", "ec2:DescribeSecurityGroups", "ec2:DescribeSubnets",
      "ec2:DescribeVpcs", "ec2:DescribeVpcAttribute", "ec2:DescribeVolumes", "ec2:DetachInternetGateway",
      "ec2:DisassociateRouteTable", "ec2:DisassociateAddress", "ec2:ModifyInstanceAttribute",
      "ec2:ModifyNetworkInterfaceAttribute", "ec2:ModifySubnetAttribute", "ec2:ReleaseAddress",
      "ec2:RevokeSecurityGroupIngress", "ec2:RunInstances", "ec2:TerminateInstances",
      "tag:GetResources", "elasticloadbalancing:AddTags", "elasticloadbalancing:CreateLoadBalancer",
      "elasticloadbalancing:ConfigureHealthCheck", "elasticloadbalancing:DeleteLoadBalancer",
      "elasticloadbalancing:DescribeLoadBalancers", "elasticloadbalancing:DescribeLoadBalancerAttributes",
      "elasticloadbalancing:DescribeTags", "elasticloadbalancing:ModifyLoadBalancerAttributes",
      "elasticloadbalancing:RegisterInstancesWithLoadBalancer", "elasticloadbalancing:DeregisterInstancesFromLoadBalancer",
      "elasticloadbalancing:RemoveTags"], "Effect": "Allow", "Resource": ["*"]}, {"Action":
      ["iam:CreateServiceLinkedRole"], "Condition": {"StringLike": {"iam:AWSServiceName":
      "elasticloadbalancing.amazonaws.com"}}, "Effect": "Allow", "Resource": ["arn:*:iam::*:role/aws-service-role/elasticloadbalancing.amazonaws.com/AWSServiceRoleForElasticLoadBalancing"]},
      {"Action": ["iam:PassRole"], "Effect": "Allow", "Resource": ["arn:*:iam::*:role/*.tkg.cloud.abc.com"]},
      {"Action": ["secretsmanager:CreateSecret", "secretsmanager:DeleteSecret", "secretsmanager:TagResource"],
      "Effect": "Allow", "Resource": ["arn:*:secretsmanager:*:*:secret:aws.cluster.x-k8s.io/*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: controllers.tkg.cloud.abc.com
  - resource_id: arn:aws:iam::123456789012:policy/controllers.tkg.cloud.abc.com
  - id: ANPAX2FJ77DCXUDJQ2YMU
  - path: /


arn:aws:iam::123456789012:policy/controllers.tmc.cloud.abc.com:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:*", "tag:*", "elasticloadbalancing:*"],
      "Effect": "Allow", "Resource": ["*"]}, {"Action": ["secretsmanager:CreateSecret",
      "secretsmanager:DeleteSecret", "secretsmanager:TagResource"], "Effect": "Allow",
      "Resource": ["arn:aws:secretsmanager:*:*:secret:aws.cluster.x-k8s.io/*"]}, {"Action":
      ["iam:CreateServiceLinkedRole"], "Condition": {"StringLike": {"iam:AWSServiceName":
      "elasticloadbalancing.amazonaws.com"}}, "Effect": "Allow", "Resource": ["arn:aws:iam::123456789012:role/aws-service-role/elasticloadbalancing.amazonaws.com/AWSServiceRoleForElasticLoadBalancing"]},
      {"Action": ["iam:PassRole"], "Effect": "Allow", "Resource": ["arn:aws:iam::123456789012:role/*.tmc.cloud.abc.com"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: controllers.tmc.cloud.abc.com
  - resource_id: arn:aws:iam::123456789012:policy/controllers.tmc.cloud.abc.com
  - id: ANPAX2FJ77DC3B4NSRAKB
  - path: /


arn:aws:iam::123456789012:policy/get-encrypted-parameter:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["kms:Decrypt", "ssm:GetParameter"],
      "Effect": "Allow", "Resource": ["arn:aws:ssm:us-west-1:123456789012:parameter/krisi-test-secure-parameter",
      "arn:aws:kms:us-west-1:123456789012:key/alias/aws/ssm"], "Sid": "VisualEditor0"}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: get-encrypted-parameter
  - resource_id: arn:aws:iam::123456789012:policy/get-encrypted-parameter
  - id: ANPAX2FJ77DCRYJJWEDYF
  - path: /


arn:aws:iam::123456789012:policy/guardrails-vRNI-policy:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:Describe*", "logs:Describe*",
      "sts:*", "iam:ListAccountAliases", "logs:TestMetricFilter", "logs:FilterLogEvents",
      "logs:Get*"], "Effect": "Allow", "Resource": "*", "Sid": "VisualEditor0"}],
      "Version": "2012-10-17"}'
  - default_version_id: v2
  - tags:
    - Key: guardrails
      Value: ''
  - name: guardrails-vRNI-policy
  - resource_id: arn:aws:iam::123456789012:policy/guardrails-vRNI-policy
  - id: ANPAX2FJ77DCY3CBNPDRC
  - path: /


arn:aws:iam::123456789012:policy/idem-test-policy-3fb27cf8-ba7f-462a-badb-b84f1e4bac3a:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:CreateSubnet"], "Effect": "Allow",
      "Resource": "*"}], "Version": "2012-10-17"}'
  - default_version_id: v2
  - tags:
    - Key: idem-test-iam-policy-key-36a3c8e4-b01a-4719-af9f-cd2244d04fe9
      Value: idem-test-iam-policy-value-c684397b-224d-48c2-a82d-b4681b03e2fd
    - Key: Name
      Value: idem-test-policy-3fb27cf8-ba7f-462a-badb-b84f1e4bac3a
  - name: idem-test-policy-3fb27cf8-ba7f-462a-badb-b84f1e4bac3a
  - resource_id: arn:aws:iam::123456789012:policy/idem-test-policy-3fb27cf8-ba7f-462a-badb-b84f1e4bac3a
  - id: ANPAX2FJ77DCSQ2VFWIVP
  - path: /


arn:aws:iam::123456789012:policy/idem-test-policy-9b3215c6-6645-4352-bdc4-90d8f0af5c4b:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:CreateSubnet"], "Effect": "Allow",
      "Resource": "*"}], "Version": "2012-10-17"}'
  - default_version_id: v2
  - tags:
    - Key: Name
      Value: idem-test-policy-9b3215c6-6645-4352-bdc4-90d8f0af5c4b
  - name: idem-test-policy-9b3215c6-6645-4352-bdc4-90d8f0af5c4b
  - resource_id: arn:aws:iam::123456789012:policy/idem-test-policy-9b3215c6-6645-4352-bdc4-90d8f0af5c4b
  - id: ANPAX2FJ77DCXT6WJZC2P
  - path: /


arn:aws:iam::123456789012:policy/idem-test-policy-a1480c57-1776-4b12-b463-f5e0b8e276dc:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:CreateSubnet"], "Effect": "Allow",
      "Resource": "*"}], "Version": "2012-10-17"}'
  - default_version_id: v2
  - tags:
    - Key: idem-test-iam-policy-key-fcce768d-346f-40fe-9bc4-91a2d7fa028e
      Value: idem-test-iam-policy-value-1f227395-a191-4bdc-8880-92f2fc8f8186
    - Key: Name
      Value: idem-test-policy-a1480c57-1776-4b12-b463-f5e0b8e276dc
  - name: idem-test-policy-a1480c57-1776-4b12-b463-f5e0b8e276dc
  - resource_id: arn:aws:iam::123456789012:policy/idem-test-policy-a1480c57-1776-4b12-b463-f5e0b8e276dc
  - id: ANPAX2FJ77DC3A5Q3XEHY
  - path: /


arn:aws:iam::123456789012:policy/idem-test-policy-c12916c3-052c-4cd2-9be0-13ff4e18296d:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:CreateSubnet"], "Effect": "Allow",
      "Resource": "*"}], "Version": "2012-10-17"}'
  - default_version_id: v2
  - tags:
    - Key: iam-policy-key-34a7a85d-f61a-4077-abfb-d4a9ee2f72b3
      Value: iam-policy-value-03095bce-24f5-4e98-bb39-b3d6eab5684a
    - Key: Name
      Value: idem-test-policy-c12916c3-052c-4cd2-9be0-13ff4e18296d
  - name: idem-test-policy-c12916c3-052c-4cd2-9be0-13ff4e18296d
  - resource_id: arn:aws:iam::123456789012:policy/idem-test-policy-c12916c3-052c-4cd2-9be0-13ff4e18296d
  - id: ANPAX2FJ77DC5CX4PN3L4
  - path: /


arn:aws:iam::123456789012:policy/idem-test-policy-e225f994-b47d-49b7-bb20-bde95e66947e:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:CreateVpc"], "Effect": "Allow",
      "Resource": "*"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags:
    - Key: Name
      Value: idem-test-policy-e225f994-b47d-49b7-bb20-bde95e66947e
  - name: idem-test-policy-e225f994-b47d-49b7-bb20-bde95e66947e
  - resource_id: arn:aws:iam::123456789012:policy/idem-test-policy-e225f994-b47d-49b7-bb20-bde95e66947e
  - id: ANPAX2FJ77DCTDB2ZTALD
  - path: /


arn:aws:iam::123456789012:policy/krum-ec2-autoscaling-minimum-permissions-set:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["autoscaling:AttachLoadBalancers",
      "autoscaling:DescribeAutoScalingInstances", "autoscaling:DescribePolicies",
      "autoscaling:DescribeLaunchConfigurations", "autoscaling:SuspendProcesses",
      "autoscaling:DescribeLoadBalancers", "autoscaling:PutScheduledUpdateGroupAction",
      "autoscaling:AttachInstances", "autoscaling:DeleteLaunchConfiguration", "autoscaling:DescribeAutoScalingGroups",
      "autoscaling:PutScalingPolicy", "autoscaling:UpdateAutoScalingGroup", "autoscaling:DescribeScheduledActions",
      "autoscaling:DeleteAutoScalingGroup", "autoscaling:CreateAutoScalingGroup"],
      "Effect": "Allow", "Resource": "*", "Sid": "VisualEditor0"}], "Version": "2012-10-17"}'
  - default_version_id: v5
  - tags: []
  - name: krum-ec2-autoscaling-minimum-permissions-set
  - resource_id: arn:aws:iam::123456789012:policy/krum-ec2-autoscaling-minimum-permissions-set
  - id: ANPAX2FJ77DC37NOUAVLY
  - path: /


arn:aws:iam::123456789012:policy/krum-sts-minimum-permissions-set:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect": "Allow",
      "Resource": "*", "Sid": "VisualEditor0"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: krum-sts-minimum-permissions-set
  - resource_id: arn:aws:iam::123456789012:policy/krum-sts-minimum-permissions-set
  - id: ANPAX2FJ77DC2CLWMBLXJ
  - path: /


arn:aws:iam::123456789012:policy/nodes.tkg.cloud.abc.com:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:DescribeInstances", "ec2:DescribeRegions",
      "ecr:GetAuthorizationToken", "ecr:BatchCheckLayerAvailability", "ecr:GetDownloadUrlForLayer",
      "ecr:GetRepositoryPolicy", "ecr:DescribeRepositories", "ecr:ListImages", "ecr:BatchGetImage"],
      "Effect": "Allow", "Resource": ["*"]}, {"Action": ["secretsmanager:DeleteSecret",
      "secretsmanager:GetSecretValue"], "Effect": "Allow", "Resource": ["arn:*:secretsmanager:*:*:secret:aws.cluster.x-k8s.io/*"]},
      {"Action": ["ssm:UpdateInstanceInformation", "ssmmessages:CreateControlChannel",
      "ssmmessages:CreateDataChannel", "ssmmessages:OpenControlChannel", "ssmmessages:OpenDataChannel",
      "s3:GetEncryptionConfiguration"], "Effect": "Allow", "Resource": ["*"]}], "Version":
      "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: nodes.tkg.cloud.abc.com
  - resource_id: arn:aws:iam::123456789012:policy/nodes.tkg.cloud.abc.com
  - id: ANPAX2FJ77DCVU7JFMYF2
  - path: /


arn:aws:iam::123456789012:policy/nodes.tmc.cloud.abc.com:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:DescribeInstances", "ec2:DescribeRegions",
      "ecr:GetAuthorizationToken", "ecr:BatchCheckLayerAvailability", "ecr:GetDownloadUrlForLayer",
      "ecr:GetRepositoryPolicy", "ecr:DescribeRepositories", "ecr:ListImages", "ecr:BatchGetImage"],
      "Effect": "Allow", "Resource": ["*"]}, {"Action": ["secretsmanager:DeleteSecret",
      "secretsmanager:GetSecretValue"], "Effect": "Allow", "Resource": ["arn:aws:secretsmanager:*:*:secret:aws.cluster.x-k8s.io/*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: nodes.tmc.cloud.abc.com
  - resource_id: arn:aws:iam::123456789012:policy/nodes.tmc.cloud.abc.com
  - id: ANPAX2FJ77DC5QCMKSOWO
  - path: /


arn:aws:iam::123456789012:policy/org1_cluster01_potato_dev_k8s_admins:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "xyz:DescribeCluster", "Effect": "Allow",
      "Resource": "arn:aws:xyz:us-west-2:123456789012:cluster/idem-test"}], "Version":
      "2012-10-17"}'
  - default_version_id: v2
  - tags: []
  - name: org1_cluster01_potato_dev_k8s_admins
  - resource_id: arn:aws:iam::123456789012:policy/org1_cluster01_potato_dev_k8s_admins
  - id: ANPAX2FJ77DC2ZZ2FCW4I
  - path: /


arn:aws:iam::123456789012:policy/org1_cluster01_potato_dev_org1_k8s_readonly:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "xyz:DescribeCluster", "Effect": "Allow",
      "Resource": "arn:aws:xyz:us-west-2:123456789012:cluster/idem-test"}], "Version":
      "2012-10-17"}'
  - default_version_id: v2
  - tags: []
  - name: org1_cluster01_potato_dev_org1_k8s_readonly
  - resource_id: arn:aws:iam::123456789012:policy/org1_cluster01_potato_dev_org1_k8s_readonly
  - id: ANPAX2FJ77DCZCK64AYYV
  - path: /


arn:aws:iam::123456789012:policy/org1_idem_test_potato_dev_k8s_admins:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "xyz:DescribeCluster", "Effect": "Allow",
      "Resource": "arn:aws:xyz:us-west-2:123456789012:cluster/idem-test"}], "Version":
      "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: org1_idem_test_potato_dev_k8s_admins
  - resource_id: arn:aws:iam::123456789012:policy/org1_idem_test_potato_dev_k8s_admins
  - id: ANPAX2FJ77DCSVDDH4UNQ
  - path: /


arn:aws:iam::123456789012:policy/org1_idem_test_potato_dev_org1_k8s_readonly:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "xyz:DescribeCluster", "Effect": "Allow",
      "Resource": "arn:aws:xyz:us-west-2:123456789012:cluster/idem-test"}], "Version":
      "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: org1_idem_test_potato_dev_org1_k8s_readonly
  - resource_id: arn:aws:iam::123456789012:policy/org1_idem_test_potato_dev_org1_k8s_readonly
  - id: ANPAX2FJ77DC5UTRYPOWD
  - path: /


arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-0e827eb6-10ae-40f8-b460-4df21de60081:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "logs:CreateLogGroup", "Effect": "Allow",
      "Resource": "arn:aws:logs:us-east-1:123456789012:*"}, {"Action": ["logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/go-test:*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLambdaBasicExecutionRole-0e827eb6-10ae-40f8-b460-4df21de60081
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-0e827eb6-10ae-40f8-b460-4df21de60081
  - id: ANPAX2FJ77DCS7BW5CPI4
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-2cbcf4ca-8169-413b-81bc-02ca66628d0d:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "logs:CreateLogGroup", "Effect": "Allow",
      "Resource": "arn:aws:logs:us-east-1:123456789012:*"}, {"Action": ["logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/go-test:*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLambdaBasicExecutionRole-2cbcf4ca-8169-413b-81bc-02ca66628d0d
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-2cbcf4ca-8169-413b-81bc-02ca66628d0d
  - id: ANPAX2FJ77DCUOL3BPYMW
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-481e19cb-d70c-47d1-9597-ca85ee2958d2:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "logs:CreateLogGroup", "Effect": "Allow",
      "Resource": "arn:aws:logs:eu-central-1:123456789012:*"}, {"Action": ["logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:eu-central-1:123456789012:log-group:/aws/lambda/myTEST:*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLambdaBasicExecutionRole-481e19cb-d70c-47d1-9597-ca85ee2958d2
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-481e19cb-d70c-47d1-9597-ca85ee2958d2
  - id: ANPAX2FJ77DCTIRFZFMSV
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-7536bc56-a15a-4ff6-bdb8-b6ed04fdfbec:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "logs:CreateLogGroup", "Effect": "Allow",
      "Resource": "arn:aws:logs:us-east-1:123456789012:*"}, {"Action": ["logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/powershell:*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLambdaBasicExecutionRole-7536bc56-a15a-4ff6-bdb8-b6ed04fdfbec
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-7536bc56-a15a-4ff6-bdb8-b6ed04fdfbec
  - id: ANPAX2FJ77DCWUZ7ZXZHH
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-77be7a89-c9ca-49c3-aac0-5d97de6b022a:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "logs:CreateLogGroup", "Effect": "Allow",
      "Resource": "arn:aws:logs:us-west-2:123456789012:*"}, {"Action": ["logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-west-2:123456789012:log-group:/aws/lambda/my-s3-function:*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLambdaBasicExecutionRole-77be7a89-c9ca-49c3-aac0-5d97de6b022a
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-77be7a89-c9ca-49c3-aac0-5d97de6b022a
  - id: ANPAX2FJ77DC6KEPGQ5QL
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-90b8d0c2-0619-4122-97a2-450841c1c21b:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogGroup", "logs:CreateLogStream",
      "logs:PutLogEvents", "logs:DescribeLogStreams"], "Effect": "Allow", "Resource":
      "arn:aws:logs:us-east-2:123456789012:*:*"}, {"Action": ["logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-2:123456789012:log-group:/aws/lambda/*:*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v3
  - tags: []
  - name: AWSLambdaBasicExecutionRole-90b8d0c2-0619-4122-97a2-450841c1c21b
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-90b8d0c2-0619-4122-97a2-450841c1c21b
  - id: ANPAIQOQODYEEYZ6K2IKA
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-9b8dc771-615a-40ed-ad1b-5a3a4b4e577f:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "logs:CreateLogGroup", "Effect": "Allow",
      "Resource": "arn:aws:logs:us-east-2:123456789012:*"}, {"Action": ["logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-2:123456789012:log-group:/aws/lambda/myFunctionName:*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLambdaBasicExecutionRole-9b8dc771-615a-40ed-ad1b-5a3a4b4e577f
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-9b8dc771-615a-40ed-ad1b-5a3a4b4e577f
  - id: ANPAJK5D2FWFQLVWTZBA2
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-ad4139c9-476f-49fa-9b5e-c3f59131819b:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "logs:CreateLogGroup", "Effect": "Allow",
      "Resource": "arn:aws:logs:us-west-2:123456789012:*"}, {"Action": ["logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-west-2:123456789012:log-group:/aws/lambda/idem-test:*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLambdaBasicExecutionRole-ad4139c9-476f-49fa-9b5e-c3f59131819b
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-ad4139c9-476f-49fa-9b5e-c3f59131819b
  - id: ANPAX2FJ77DC3UFZ6PKNP
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-b7f13f4b-0583-447f-a37d-8319109d2155:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "logs:CreateLogGroup", "Effect": "Allow",
      "Resource": "arn:aws:logs:us-east-2:123456789012:*"}, {"Action": ["logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-2:123456789012:log-group:/aws/lambda/rnitin-lambda-sample1:*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLambdaBasicExecutionRole-b7f13f4b-0583-447f-a37d-8319109d2155
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-b7f13f4b-0583-447f-a37d-8319109d2155
  - id: ANPAX2FJ77DCVGZP633OC
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-c115d523-c25b-44e3-b42b-32143f4af4bf:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "logs:CreateLogGroup", "Effect": "Allow",
      "Resource": "arn:aws:logs:us-east-1:123456789012:*"}, {"Action": ["logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/t1:*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLambdaBasicExecutionRole-c115d523-c25b-44e3-b42b-32143f4af4bf
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-c115d523-c25b-44e3-b42b-32143f4af4bf
  - id: ANPAX2FJ77DCWHGZZ6T5F
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-c5399330-733d-477d-b360-10358e90ba73:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "logs:CreateLogGroup", "Effect": "Allow",
      "Resource": "arn:aws:logs:us-east-1:123456789012:*"}, {"Action": ["logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/InstanceTypeCheck:*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLambdaBasicExecutionRole-c5399330-733d-477d-b360-10358e90ba73
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-c5399330-733d-477d-b360-10358e90ba73
  - id: ANPAX2FJ77DC7P4W7OWDX
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-f28418d0-fcc1-413a-9443-7eda82a55427:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "logs:CreateLogGroup", "Effect": "Allow",
      "Resource": "arn:aws:logs:eu-central-1:123456789012:*"}, {"Action": ["logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:eu-central-1:123456789012:log-group:/aws/lambda/myFirstAPI:*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLambdaBasicExecutionRole-f28418d0-fcc1-413a-9443-7eda82a55427
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-f28418d0-fcc1-413a-9443-7eda82a55427
  - id: ANPAX2FJ77DCZQQRC7QU6
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-f35de733-79b9-4ffc-9ef2-1afaa15f52c8:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "logs:CreateLogGroup", "Effect": "Allow",
      "Resource": "arn:aws:logs:us-east-1:123456789012:*"}, {"Action": ["logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/java-test:*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLambdaBasicExecutionRole-f35de733-79b9-4ffc-9ef2-1afaa15f52c8
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-f35de733-79b9-4ffc-9ef2-1afaa15f52c8
  - id: ANPAX2FJ77DCTVDKPNBR7
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/AWSLambdaS3ExecutionRole-7507ff52-f9ad-40bf-8f91-bcfc501db9e6:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["s3:GetObject"], "Effect": "Allow",
      "Resource": "arn:aws:s3:::*"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLambdaS3ExecutionRole-7507ff52-f9ad-40bf-8f91-bcfc501db9e6
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AWSLambdaS3ExecutionRole-7507ff52-f9ad-40bf-8f91-bcfc501db9e6
  - id: ANPAX2FJ77DCUBQFXA46X
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/AmazonSageMaker-ExecutionPolicy-20180207T165162:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["s3:GetObject", "s3:PutObject", "s3:DeleteObject",
      "s3:ListBucket"], "Effect": "Allow", "Resource": ["arn:aws:s3:::*"]}], "Version":
      "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AmazonSageMaker-ExecutionPolicy-20180207T165162
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AmazonSageMaker-ExecutionPolicy-20180207T165162
  - id: ANPAIERQNST7U6EUC6BVW
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/ProtonRolePolicy-test:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["cloudformation:CancelUpdateStack",
      "cloudformation:ContinueUpdateRollback", "cloudformation:CreateChangeSet", "cloudformation:CreateStack",
      "cloudformation:DeleteChangeSet", "cloudformation:DeleteStack", "cloudformation:DescribeChangeSet",
      "cloudformation:DescribeStackDriftDetectionStatus", "cloudformation:DescribeStackEvents",
      "cloudformation:DescribeStackResourceDrifts", "cloudformation:DescribeStacks",
      "cloudformation:DetectStackResourceDrift", "cloudformation:ExecuteChangeSet",
      "cloudformation:ListChangeSets", "cloudformation:ListStackResources", "cloudformation:UpdateStack"],
      "Effect": "Allow", "Resource": "arn:aws:cloudformation:*:123456789012:stack/AWSProton-*"},
      {"Condition": {"ForAnyValue:StringEquals": {"aws:CalledVia": ["cloudformation.amazonaws.com"]}},
      "Effect": "Allow", "NotAction": ["organizations:*", "account:*"], "Resource":
      "*"}, {"Action": ["organizations:DescribeOrganization", "account:ListRegions"],
      "Condition": {"ForAnyValue:StringEquals": {"aws:CalledVia": ["cloudformation.amazonaws.com"]}},
      "Effect": "Allow", "Resource": "*"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: ProtonRolePolicy-test
  - resource_id: arn:aws:iam::123456789012:policy/service-role/ProtonRolePolicy-test
  - id: ANPAX2FJ77DCQ6NJ3BXDI
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/StatesExecutionPolicy:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["lambda:InvokeFunction"], "Effect":
      "Allow", "Resource": "*"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: StatesExecutionPolicy
  - resource_id: arn:aws:iam::123456789012:policy/service-role/StatesExecutionPolicy
  - id: ANPAIHMYKKWSGBHYVB5FW
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/config-role-us-east-1_AWSConfigDeliveryPermissions_us-east-1:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["s3:PutObject*"], "Condition": {"StringLike":
      {"s3:x-amz-acl": "bucket-owner-full-control"}}, "Effect": "Allow", "Resource":
      ["arn:aws:s3:::config-bucket-123456789012/AWSLogs/123456789012/*"]}, {"Action":
      ["s3:GetBucketAcl"], "Effect": "Allow", "Resource": "arn:aws:s3:::config-bucket-123456789012"}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: config-role-us-east-1_AWSConfigDeliveryPermissions_us-east-1
  - resource_id: arn:aws:iam::123456789012:policy/service-role/config-role-us-east-1_AWSConfigDeliveryPermissions_us-east-1
  - id: ANPAI6UT23RYY2ONIP5SY
  - path: /service-role/


arn:aws:iam::123456789012:policy/spotinst-iam-stack-33rom-SpotinstManagedPolicy-1VGQO1DZRBAM8:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:RequestSpotInstances", "ec2:CancelSpotInstanceRequests",
      "ec2:CreateSpotDatafeedSubscription", "ec2:Describe*", "ec2:AssociateAddress",
      "ec2:AttachVolume", "ec2:ConfirmProductInstance", "ec2:CopyImage", "ec2:CopySnapshot",
      "ec2:CreateImage", "ec2:CreateSnapshot", "ec2:CreateTags", "ec2:CreateVolume",
      "ec2:DeleteTags", "ec2:DisassociateAddress", "ec2:ModifyImageAttribute", "ec2:ModifyInstanceAttribute",
      "ec2:MonitorInstances", "ec2:RebootInstances", "ec2:RegisterImage", "ec2:RunInstances",
      "ec2:StartInstances", "ec2:StopInstances", "ec2:TerminateInstances", "ec2:UnassignPrivateIpAddresses",
      "ec2:DeregisterImage", "ec2:DeleteSnapshot", "ec2:DeleteVolume", "ec2:ModifyReservedInstances",
      "ec2:CreateReservedInstancesListing", "ec2:CancelReservedInstancesListing",
      "ec2:ModifyNetworkInterfaceAttribute", "ec2:DeleteNetworkInterface"], "Effect":
      "Allow", "Resource": ["*"], "Sid": "GeneralSpotInstancesAccess"}, {"Action":
      ["elasticloadbalancing:Describe*", "elasticloadbalancing:Deregister*", "elasticloadbalancing:Register*",
      "elasticloadbalancing:RemoveTags", "elasticloadbalancing:RegisterTargets", "elasticloadbalancing:EnableAvailabilityZonesForLoadBalancer",
      "elasticloadbalancing:DisableAvailabilityZonesForLoadBalancer", "elasticloadbalancing:DescribeTags",
      "elasticloadbalancing:CreateTargetGroup", "elasticloadbalancing:DeleteTargetGroup",
      "elasticloadbalancing:ModifyRule", "elasticloadbalancing:AddTags", "elasticloadbalancing:ModifyTargetGroupAttributes",
      "elasticloadbalancing:ModifyTargetGroup", "elasticloadbalancing:ModifyListener"],
      "Effect": "Allow", "Resource": ["*"], "Sid": "AccessELB"}, {"Action": ["cloudwatch:DescribeAlarmHistory",
      "cloudwatch:DescribeAlarms", "cloudwatch:DescribeAlarmsForMetric", "cloudwatch:GetMetricStatistics",
      "cloudwatch:GetMetricData", "cloudwatch:ListMetrics", "cloudwatch:PutMetricData",
      "cloudwatch:PutMetricAlarm"], "Effect": "Allow", "Resource": ["*"], "Sid": "AccessCloudWatch"},
      {"Action": ["sns:Publish", "sns:ListTopics", "sns:CreateTopic", "sns:GetTopicAttributes",
      "sns:ListSubscriptionsByTopic", "sns:Subscribe"], "Effect": "Allow", "Resource":
      ["*"], "Sid": "AccessSNS"}, {"Action": ["iam:AddRoleToInstanceProfile", "iam:ListInstanceProfiles",
      "iam:ListInstanceProfilesForRole", "iam:PassRole", "iam:ListRoles", "iam:ListAccountAliases",
      "iam:GetPolicyVersion", "iam:ListPolicies", "iam:GetPolicy", "iam:ListAttachedRolePolicies",
      "organizations:ListAccounts", "iam:CreateServiceLinkedRole", "iam:PutRolePolicy",
      "iam:GetInstanceProfile", "iam:GetRolePolicy", "iam:ListRolePolicies", "iam:SimulatePrincipalPolicy"],
      "Effect": "Allow", "Resource": ["*"], "Sid": "AccessIAM"}, {"Action": ["elasticbeanstalk:Describe*",
      "elasticbeanstalk:RequestEnvironmentInfo", "elasticbeanstalk:RetrieveEnvironmentInfo",
      "elasticbeanstalk:ValidateConfigurationSettings", "elasticbeanstalk:UpdateEnvironment",
      "elasticbeanstalk:ListPlatformVersions", "cloudformation:GetTemplate", "cloudformation:DescribeStackResources",
      "cloudformation:DescribeStackResource", "cloudformation:DescribeStacks", "cloudformation:ListStackResources",
      "cloudformation:UpdateStack", "cloudformation:DescribeStackEvents", "logs:PutRetentionPolicy",
      "logs:createLogGroup", "elasticbeanstalk:ListTagsForResource"], "Effect": "Allow",
      "Resource": ["*"], "Sid": "GeneralAccessElaticBeanstalk"}, {"Action": ["autoscaling:*"],
      "Effect": "Allow", "Resource": ["*"], "Sid": "AccessAutoScalingGroups"}, {"Action":
      ["xyz:ListClusters", "xyz:DescribeNodegroup", "xyz:ListNodegroups"], "Effect":
      "Allow", "Resource": ["*"], "Sid": "Accessxyz"}, {"Action": ["elasticmapreduce:*",
      "s3:GetObject"], "Effect": "Allow", "Resource": ["*"], "Sid": "AccessEMR"},
      {"Action": ["ecs:List*", "ecs:Describe*", "ecs:DeregisterContainerInstance",
      "ecs:UpdateContainerInstancesState", "ecs:RegisterTaskDefinition", "ecs:CreateService",
      "application-autoscaling:PutScalingPolicy", "application-autoscaling:RegisterScalableTarget",
      "application-autoscaling:Describe*", "ecs:putAttributes"], "Effect": "Allow",
      "Resource": ["*"], "Sid": "AccessECS"}, {"Action": ["batch:List*", "batch:Describe*"],
      "Effect": "Allow", "Resource": ["*"], "Sid": "AccessBatch"}, {"Action": ["opsworks:DeregisterInstance",
      "opsworks:DescribeInstances", "opsworks:DescribeStacks", "opsworks:DescribeLayers"],
      "Effect": "Allow", "Resource": ["*"], "Sid": "AccessOpsWorks"}, {"Action": ["codedeploy:*"],
      "Effect": "Allow", "Resource": ["*"], "Sid": "AccessCodeDeploy"}, {"Action":
      ["s3:GetObject", "s3:List*", "s3:GetBucketLocation"], "Effect": "Allow", "Resource":
      ["*"], "Sid": "AccessGeneralS3"}, {"Action": ["route53:ListHostedZones", "route53:ListResourceRecordSets",
      "route53:ChangeResourceRecordSets"], "Effect": "Allow", "Resource": ["*"], "Sid":
      "AccessRoute53"}, {"Action": ["s3:*"], "Effect": "Allow", "Resource": ["arn:aws:s3:::elasticbeanstalk*"],
      "Sid": "AccesS3forElasticBeanstalk"}, {"Action": ["ecs:Poll", "ecs:DiscoverPollEndpoint",
      "ecs:StartTelemetrySession", "ecs:StartTask", "ecs:StopTask", "ecs:DescribeContainerInstances",
      "ecs:RegisterContainerInstance", "ecs:DeregisterContainerInstance", "ecs:SubmitContainerStateChange",
      "ecs:SubmitTaskStateChange"], "Effect": "Allow", "Resource": ["*"], "Sid": "DockerBasedBeanstalkEnvironments"},
      {"Action": ["elasticfilesystem:DescribeFileSystems"], "Effect": "Allow", "Resource":
      ["*"], "Sid": "ElasticFileSystem"}, {"Action": ["pricing:GetProducts"], "Effect":
      "Allow", "Resource": ["*"], "Sid": "Pricing"}, {"Action": ["savingsplans:Describe*",
      "savingsplans:List*"], "Effect": "Allow", "Resource": ["*"], "Sid": "SavingsPlan"},
      {"Action": ["lambda:ListFunctions"], "Effect": "Allow", "Resource": ["*"], "Sid":
      "Lambda"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: spotinst-iam-stack-33rom-SpotinstManagedPolicy-1VGQO1DZRBAM8
  - resource_id: arn:aws:iam::123456789012:policy/spotinst-iam-stack-33rom-SpotinstManagedPolicy-1VGQO1DZRBAM8
  - id: ANPAX2FJ77DCS3TXKYLFL
  - path: /


arn:aws:iam::123456789012:policy/spotinst-iam-stack-8tt4w-SpotinstManagedPolicy-6URGPL3AFJO8:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:RequestSpotInstances", "ec2:CancelSpotInstanceRequests",
      "ec2:CreateSpotDatafeedSubscription", "ec2:Describe*", "ec2:AssociateAddress",
      "ec2:AttachVolume", "ec2:ConfirmProductInstance", "ec2:CopyImage", "ec2:CopySnapshot",
      "ec2:CreateImage", "ec2:CreateSnapshot", "ec2:CreateTags", "ec2:CreateVolume",
      "ec2:DeleteTags", "ec2:DisassociateAddress", "ec2:ModifyImageAttribute", "ec2:ModifyInstanceAttribute",
      "ec2:MonitorInstances", "ec2:RebootInstances", "ec2:RegisterImage", "ec2:RunInstances",
      "ec2:StartInstances", "ec2:StopInstances", "ec2:TerminateInstances", "ec2:UnassignPrivateIpAddresses",
      "ec2:DeregisterImage", "ec2:DeleteSnapshot", "ec2:DeleteVolume", "ec2:ModifyReservedInstances",
      "ec2:CreateReservedInstancesListing", "ec2:CancelReservedInstancesListing",
      "ec2:ModifyNetworkInterfaceAttribute", "ec2:DeleteNetworkInterface"], "Effect":
      "Allow", "Resource": ["*"], "Sid": "GeneralSpotInstancesAccess"}, {"Action":
      ["elasticloadbalancing:Describe*", "elasticloadbalancing:Deregister*", "elasticloadbalancing:Register*",
      "elasticloadbalancing:RemoveTags", "elasticloadbalancing:RegisterTargets", "elasticloadbalancing:EnableAvailabilityZonesForLoadBalancer",
      "elasticloadbalancing:DisableAvailabilityZonesForLoadBalancer", "elasticloadbalancing:DescribeTags",
      "elasticloadbalancing:CreateTargetGroup", "elasticloadbalancing:DeleteTargetGroup",
      "elasticloadbalancing:ModifyRule", "elasticloadbalancing:AddTags", "elasticloadbalancing:ModifyTargetGroupAttributes",
      "elasticloadbalancing:ModifyTargetGroup", "elasticloadbalancing:ModifyListener"],
      "Effect": "Allow", "Resource": ["*"], "Sid": "AccessELB"}, {"Action": ["cloudwatch:DescribeAlarmHistory",
      "cloudwatch:DescribeAlarms", "cloudwatch:DescribeAlarmsForMetric", "cloudwatch:GetMetricStatistics",
      "cloudwatch:GetMetricData", "cloudwatch:ListMetrics", "cloudwatch:PutMetricData",
      "cloudwatch:PutMetricAlarm"], "Effect": "Allow", "Resource": ["*"], "Sid": "AccessCloudWatch"},
      {"Action": ["sns:Publish", "sns:ListTopics", "sns:CreateTopic", "sns:GetTopicAttributes",
      "sns:ListSubscriptionsByTopic", "sns:Subscribe"], "Effect": "Allow", "Resource":
      ["*"], "Sid": "AccessSNS"}, {"Action": ["iam:AddRoleToInstanceProfile", "iam:ListInstanceProfiles",
      "iam:ListInstanceProfilesForRole", "iam:PassRole", "iam:ListRoles", "iam:ListAccountAliases",
      "iam:GetPolicyVersion", "iam:ListPolicies", "iam:GetPolicy", "iam:ListAttachedRolePolicies",
      "organizations:ListAccounts", "iam:CreateServiceLinkedRole", "iam:PutRolePolicy",
      "iam:GetInstanceProfile", "iam:GetRolePolicy", "iam:ListRolePolicies", "iam:SimulatePrincipalPolicy"],
      "Effect": "Allow", "Resource": ["*"], "Sid": "AccessIAM"}, {"Action": ["elasticbeanstalk:Describe*",
      "elasticbeanstalk:RequestEnvironmentInfo", "elasticbeanstalk:RetrieveEnvironmentInfo",
      "elasticbeanstalk:ValidateConfigurationSettings", "elasticbeanstalk:UpdateEnvironment",
      "elasticbeanstalk:ListPlatformVersions", "cloudformation:GetTemplate", "cloudformation:DescribeStackResources",
      "cloudformation:DescribeStackResource", "cloudformation:DescribeStacks", "cloudformation:ListStackResources",
      "cloudformation:UpdateStack", "cloudformation:DescribeStackEvents", "logs:PutRetentionPolicy",
      "logs:createLogGroup", "elasticbeanstalk:ListTagsForResource"], "Effect": "Allow",
      "Resource": ["*"], "Sid": "GeneralAccessElaticBeanstalk"}, {"Action": ["autoscaling:*"],
      "Effect": "Allow", "Resource": ["*"], "Sid": "AccessAutoScalingGroups"}, {"Action":
      ["xyz:ListClusters", "xyz:DescribeNodegroup", "xyz:ListNodegroups"], "Effect":
      "Allow", "Resource": ["*"], "Sid": "Accessxyz"}, {"Action": ["elasticmapreduce:*",
      "s3:GetObject"], "Effect": "Allow", "Resource": ["*"], "Sid": "AccessEMR"},
      {"Action": ["ecs:List*", "ecs:Describe*", "ecs:DeregisterContainerInstance",
      "ecs:UpdateContainerInstancesState", "ecs:RegisterTaskDefinition", "ecs:CreateService",
      "application-autoscaling:PutScalingPolicy", "application-autoscaling:RegisterScalableTarget",
      "application-autoscaling:Describe*", "ecs:putAttributes"], "Effect": "Allow",
      "Resource": ["*"], "Sid": "AccessECS"}, {"Action": ["batch:List*", "batch:Describe*"],
      "Effect": "Allow", "Resource": ["*"], "Sid": "AccessBatch"}, {"Action": ["opsworks:DeregisterInstance",
      "opsworks:DescribeInstances", "opsworks:DescribeStacks", "opsworks:DescribeLayers"],
      "Effect": "Allow", "Resource": ["*"], "Sid": "AccessOpsWorks"}, {"Action": ["codedeploy:*"],
      "Effect": "Allow", "Resource": ["*"], "Sid": "AccessCodeDeploy"}, {"Action":
      ["s3:GetObject", "s3:List*", "s3:GetBucketLocation"], "Effect": "Allow", "Resource":
      ["*"], "Sid": "AccessGeneralS3"}, {"Action": ["route53:ListHostedZones", "route53:ListResourceRecordSets",
      "route53:ChangeResourceRecordSets"], "Effect": "Allow", "Resource": ["*"], "Sid":
      "AccessRoute53"}, {"Action": ["s3:*"], "Effect": "Allow", "Resource": ["arn:aws:s3:::elasticbeanstalk*"],
      "Sid": "AccesS3forElasticBeanstalk"}, {"Action": ["ecs:Poll", "ecs:DiscoverPollEndpoint",
      "ecs:StartTelemetrySession", "ecs:StartTask", "ecs:StopTask", "ecs:DescribeContainerInstances",
      "ecs:RegisterContainerInstance", "ecs:DeregisterContainerInstance", "ecs:SubmitContainerStateChange",
      "ecs:SubmitTaskStateChange"], "Effect": "Allow", "Resource": ["*"], "Sid": "DockerBasedBeanstalkEnvironments"},
      {"Action": ["elasticfilesystem:DescribeFileSystems"], "Effect": "Allow", "Resource":
      ["*"], "Sid": "ElasticFileSystem"}, {"Action": ["pricing:GetProducts"], "Effect":
      "Allow", "Resource": ["*"], "Sid": "Pricing"}, {"Action": ["savingsplans:Describe*",
      "savingsplans:List*"], "Effect": "Allow", "Resource": ["*"], "Sid": "SavingsPlan"},
      {"Action": ["lambda:ListFunctions"], "Effect": "Allow", "Resource": ["*"], "Sid":
      "Lambda"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: spotinst-iam-stack-8tt4w-SpotinstManagedPolicy-6URGPL3AFJO8
  - resource_id: arn:aws:iam::123456789012:policy/spotinst-iam-stack-8tt4w-SpotinstManagedPolicy-6URGPL3AFJO8
  - id: ANPAX2FJ77DC3HUBK77FV
  - path: /


arn:aws:iam::123456789012:policy/test_policy:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "account:DeleteVpc", "Effect": "Allow",
      "Resource": "*", "Sid": "VisualEditor0"}], "Version": "2012-10-17"}'
  - default_version_id: v3
  - tags:
    - Key: MP2
      Value: tag-2
  - name: test_policy
  - resource_id: arn:aws:iam::123456789012:policy/test_policy
  - id: ANPAX2FJ77DCURKJFXYIV
  - path: /


arn:aws:iam::123456789012:policy/vmw-cloudhealth-automation:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:DeleteSnapshot"], "Effect":
      "Allow", "Resource": "*"}, {"Action": ["ec2:DeleteVolume"], "Effect": "Allow",
      "Resource": "*"}, {"Action": ["ec2:ModifyReservedInstances"], "Effect": "Allow",
      "Resource": "*"}, {"Action": ["ec2:DescribeReservedInstancesOfferings", "ec2:PurchaseReservedInstancesOffering",
      "sts:GetFederationToken"], "Effect": "Allow", "Resource": "*"}, {"Action": ["rds:DescribeReservedDBInstancesOfferings",
      "rds:PurchaseReservedDBInstancesOffering"], "Effect": "Allow", "Resource": "*"},
      {"Action": ["lambda:InvokeFunction"], "Effect": "Allow", "Resource": "*"}, {"Action":
      ["ec2:ReleaseAddress"], "Effect": "Allow", "Resource": "*"}, {"Action": ["ec2:CreateSnapshot"],
      "Effect": "Allow", "Resource": "*"}, {"Action": ["ec2:ModifyReservedInstances",
      "ec2:DescribeReservedInstancesOfferings", "ec2:GetReservedInstancesExchangeQuote",
      "ec2:AcceptReservedInstancesExchangeQuote"], "Effect": "Allow", "Resource":
      "*"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: vmw-cloudhealth-automation
  - resource_id: arn:aws:iam::123456789012:policy/vmw-cloudhealth-automation
  - id: ANPAICPYXZBIW3Y35O6SI
  - path: /


arn:aws:iam::123456789012:policy/vmw-cloudhealth-policy:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["aws-portal:ViewBilling", "aws-portal:ViewUsage",
      "autoscaling:Describe*", "cloudformation:ListStacks", "cloudformation:ListStackResources",
      "cloudformation:DescribeStacks", "cloudformation:DescribeStackEvents", "cloudformation:DescribeStackResources",
      "cloudformation:GetTemplate", "cloudfront:Get*", "cloudfront:List*", "cloudtrail:DescribeTrails",
      "cloudtrail:ListTags", "cloudwatch:Describe*", "cloudwatch:Get*", "cloudwatch:List*",
      "config:Get*", "config:Describe*", "config:Deliver*", "config:List*", "cur:Describe*",
      "dynamodb:DescribeTable", "dynamodb:List*", "ec2:Describe*", "elasticache:Describe*",
      "elasticache:ListTagsForResource", "elasticbeanstalk:Check*", "elasticbeanstalk:Describe*",
      "elasticbeanstalk:List*", "elasticbeanstalk:RequestEnvironmentInfo", "elasticbeanstalk:RetrieveEnvironmentInfo",
      "elasticfilesystem:Describe*", "elasticloadbalancing:Describe*", "elasticmapreduce:Describe*",
      "elasticmapreduce:List*", "es:List*", "es:Describe*", "iam:List*", "iam:Get*",
      "iam:GenerateCredentialReport", "lambda:List*", "redshift:Describe*", "route53:Get*",
      "route53:List*", "rds:Describe*", "rds:ListTagsForResource", "s3:List*", "s3:GetBucketTagging",
      "s3:GetBucketLocation", "s3:GetBucketLogging", "s3:GetBucketVersioning", "s3:GetBucketWebsite",
      "sdb:GetAttributes", "sdb:List*", "ses:Get*", "ses:List*", "sns:Get*", "sns:List*",
      "sqs:GetQueueAttributes", "sqs:ListQueues", "storagegateway:List*", "storagegateway:Describe*",
      "workspaces:Describe*", "kinesis:Describe*", "kinesis:List*", "firehose:DescribeDeliveryStream",
      "firehose:ListDeliveryStreams"], "Effect": "Allow", "Resource": "*"}], "Version":
      "2012-10-17"}'
  - default_version_id: v3
  - tags: []
  - name: vmw-cloudhealth-policy
  - resource_id: arn:aws:iam::123456789012:policy/vmw-cloudhealth-policy
  - id: ANPAIPCUZFTQNXYIPTMWQ
  - path: /


arn:aws:iam::123456789012:policy/xyz-idem-test-admin:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["xyz:*"], "Effect": "Allow", "Resource":
      "*"}, {"Action": ["iam:PassRole"], "Effect": "Allow", "Resource": ["arn:aws:iam::123456789012:role/idem-test-temp-xyz-cluster"]},
      {"Action": ["kms:Create*", "kms:Describe*", "kms:Enable*", "kms:List*", "kms:Put*",
      "kms:Update*", "kms:Revoke*", "kms:Disable*", "kms:Get*", "kms:Delete*", "kms:TagResource",
      "kms:UntagResource", "kms:ScheduleKeyDeletion", "kms:CancelKeyDeletion"], "Effect":
      "Allow", "Resource": "arn:aws:kms:eu-west-3:123456789012:key/8ac5f341-fd1c-4e9d-9596-8f844dba5cc8"}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: xyz-idem-test-admin
  - resource_id: arn:aws:iam::123456789012:policy/xyz-idem-test-admin
  - id: ANPAX2FJ77DCY4DVSXP5E
  - path: /


arn:aws:iam::123456789012:policy/xyz-idem-test-jenkins:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "xyz:DescribeCluster", "Effect": "Allow",
      "Resource": "arn:aws:xyz:eu-west-3:123456789012:cluster/idem-test"}], "Version":
      "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: xyz-idem-test-jenkins
  - resource_id: arn:aws:iam::123456789012:policy/xyz-idem-test-jenkins
  - id: ANPAX2FJ77DCWXVXTSXMQ
  - path: /
