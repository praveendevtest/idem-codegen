ensemble_test-ensemble_primary_acc_policy:
  aws.iam.user_policy.present:
  - resource_id: ensemble_test-ensemble_primary_acc_policy
  - user_name: ensemble_test
  - name: ensemble_primary_acc_policy
  - policy_document: '{"Statement": [{"Action": ["iam:ListAccountAliases"], "Effect":
      "Allow", "Resource": ["*"]}, {"Action": ["ec2:Describe*"], "Effect": "Allow",
      "Resource": "*"}, {"Action": ["logs:Describe*", "logs:Get*", "logs:TestMetricFilter",
      "logs:FilterLogEvents"], "Effect": "Allow", "Resource": "*"}, {"Action": ["organizations:ListAccounts"],
      "Effect": "Allow", "Resource": "*"}, {"Action": "sts:AssumeRole", "Effect":
      "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


extension-jenkins-idem-test-extension-jenkins-idem-test:
  aws.iam.user_policy.present:
  - resource_id: extension-jenkins-idem-test-extension-jenkins-idem-test
  - user_name: extension-jenkins-idem-test
  - name: extension-jenkins-idem-test
  - policy_document: '{"Statement": [{"Action": ["xyz:DescribeCluster"], "Effect":
      "Allow", "Resource": "arn:aws:xyz:eu-west-3:123456789012:cluster/idem-test"},
      {"Action": ["sts:AssumeRole"], "Effect": "Allow", "Resource": ["arn:aws:iam::123456789012:role/xyz-idem-test-jenkins"],
      "Sid": ""}, {"Action": "s3:*", "Effect": "Allow", "Resource": ["arn:aws:s3:::ssm-ansible-test-dev",
      "arn:aws:s3:::ssm-ansible-test-dev/*"], "Sid": ""}, {"Action": ["ssm:StartSession"],
      "Condition": {"StringLike": {"ssm:resourceTag/KubernetesCluster": ["idem-test"]}},
      "Effect": "Allow", "Resource": "*"}, {"Action": ["ssm:TerminateSession"], "Effect":
      "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


extension-jenkins-idem-test-extension-jenkins-rolling-upgrade-idem-test:
  aws.iam.user_policy.present:
  - resource_id: extension-jenkins-idem-test-extension-jenkins-rolling-upgrade-idem-test
  - user_name: extension-jenkins-idem-test
  - name: extension-jenkins-rolling-upgrade-idem-test
  - policy_document: '{"Statement": [{"Action": "ec2:Describe*", "Effect": "Allow",
      "Resource": "*"}, {"Action": ["sts:AssumeRole"], "Effect": "Allow", "Resource":
      ["arn:aws:iam::123456789012:role/xyz-idem-test-jenkins"], "Sid": ""}, {"Action":
      ["autoscaling:DeleteTags", "autoscaling:ResumeProcesses", "autoscaling:CreateOrUpdateTags",
      "autoscaling:UpdateAutoScalingGroup", "autoscaling:SuspendProcesses", "autoscaling:TerminateInstanceInAutoScalingGroup"],
      "Condition": {"StringEquals": {"autoscaling:ResourceTag/KubernetesCluster":
      "idem-test"}}, "Effect": "Allow", "Resource": "*", "Sid": ""}, {"Action": ["xyz:UpdateClusterVersion",
      "ec2:DescribeInstances", "ec2:RebootInstances", "autoscaling:DescribeAutoScalingGroups",
      "xyz:DescribeUpdate", "xyz:DescribeCluster", "xyz:ListClusters", "xyz:CreateCluster"],
      "Effect": "Allow", "Resource": "*", "Sid": ""}, {"Action": "ec2:DescribeInstances",
      "Effect": "Allow", "Resource": "*", "Sid": ""}], "Version": "2012-10-17"}'


idem-s3-idem-s332:
  aws.iam.user_policy.present:
  - resource_id: idem-s3-idem-s332
  - user_name: idem-s3
  - name: idem-s332
  - policy_document: '{"Statement": [{"Action": ["s3:*", "s3-object-lambda:*"], "Effect":
      "Allow", "Resource": "arn:aws:s3:::idem-s3-test"}], "Version": "2012-10-17"}'


idem_aws_demo_user-idem-s3:
  aws.iam.user_policy.present:
  - resource_id: idem_aws_demo_user-idem-s3
  - user_name: idem_aws_demo_user
  - name: idem-s3
  - policy_document: '{"Statement": [{"Action": ["s3:*", "s3-object-lambda:*"], "Effect":
      "Deny", "Resource": "arn:aws:s3:::idem-s3-test"}], "Version": "2012-10-17"}'


prelude_test-potato-AWS-Permissions:
  aws.iam.user_policy.present:
  - resource_id: prelude_test-potato-AWS-Permissions
  - user_name: prelude_test
  - name: potato-AWS-Permissions
  - policy_document: '{"Statement": [{"Action": ["ec2:DescribeAvailabilityZones",
      "ec2:DescribeInstances", "ec2:DescribeInternetGateways", "ec2:DescribeRouteTables",
      "ec2:DescribeSecurityGroups", "ec2:DescribeSubnets", "ec2:DescribeVolumes",
      "ec2:DescribeVpcs"], "Effect": "Allow", "Resource": "*"}, {"Action": ["cloudwatch:ListMetrics",
      "cloudwatch:GetMetricStatistics"], "Effect": "Allow", "Resource": "*"}, {"Action":
      ["s3:Get*", "s3:List*"], "Effect": "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


prelude_test02-ensemble_primary_acc_policy-test:
  aws.iam.user_policy.present:
  - resource_id: prelude_test02-ensemble_primary_acc_policy-test
  - user_name: prelude_test02
  - name: ensemble_primary_acc_policy-test
  - policy_document: '{"Statement": [{"Action": ["iam:ListAccountAliases"], "Effect":
      "Allow", "Resource": ["*"]}, {"Action": ["ec2:Describe*"], "Effect": "Allow",
      "Resource": "*"}, {"Action": ["logs:Describe*", "logs:Get*", "logs:TestMetricFilter",
      "logs:FilterLogEvents"], "Effect": "Allow", "Resource": "*"}, {"Action": ["organizations:ListAccounts"],
      "Effect": "Allow", "Resource": "*"}, {"Action": "sts:AssumeRole", "Effect":
      "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


prelude_test02-potato-AWS-Permissions:
  aws.iam.user_policy.present:
  - resource_id: prelude_test02-potato-AWS-Permissions
  - user_name: prelude_test02
  - name: potato-AWS-Permissions
  - policy_document: '{"Statement": [{"Action": ["ec2:DescribeAvailabilityZones",
      "ec2:DescribeInstances", "ec2:DescribeInternetGateways", "ec2:DescribeRouteTables",
      "ec2:DescribeSecurityGroups", "ec2:DescribeSubnets", "ec2:DescribeVolumes",
      "ec2:DescribeVpcs"], "Effect": "Allow", "Resource": "*"}, {"Action": ["cloudwatch:ListMetrics",
      "cloudwatch:GetMetricStatistics"], "Effect": "Allow", "Resource": "*"}, {"Action":
      ["s3:Get*", "s3:List*"], "Effect": "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


prelude_test02-potato-AWS-Permissions1:
  aws.iam.user_policy.present:
  - resource_id: prelude_test02-potato-AWS-Permissions1
  - user_name: prelude_test02
  - name: potato-AWS-Permissions1
  - policy_document: '{"Statement": [{"Action": ["ec2:DescribeAvailabilityZones",
      "ec2:DescribeInstances", "ec2:DescribeInternetGateways", "ec2:DescribeRouteTables",
      "ec2:DescribeSecurityGroups", "ec2:DescribeSubnets", "ec2:DescribeVolumes",
      "ec2:DescribeVpcs"], "Effect": "Allow", "Resource": "*"}, {"Action": ["cloudwatch:ListMetrics",
      "cloudwatch:GetMetricStatistics"], "Effect": "Allow", "Resource": "*"}, {"Action":
      ["s3:Get*", "s3:List*"], "Effect": "Allow", "Resource": "*"}], "Version": "2012-10-17"}'
