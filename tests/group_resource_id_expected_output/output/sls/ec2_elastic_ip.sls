13.37.173.220:
  aws.ec2.elastic_ip.present:
  - name: 13.37.173.220
  - resource_id: 13.37.173.220
  - allocation_id: eipalloc-001d4219447c325ca
  - domain: vpc
  - network_border_group: eu-west-3
  - public_ipv4_pool: amazon
  - tags:
    - Key: Owner
      Value: org1
    - Key: Name
      Value: idem-test-natgw-eip-1
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test


13.38.205.2:
  aws.ec2.elastic_ip.present:
  - name: 13.38.205.2
  - resource_id: 13.38.205.2
  - allocation_id: eipalloc-01319ee06efe14298
  - domain: vpc
  - network_border_group: eu-west-3
  - public_ipv4_pool: amazon
  - tags:
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-natgw-eip-2
    - Key: Owner
      Value: org1
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test


15.236.223.139:
  aws.ec2.elastic_ip.present:
  - name: 15.236.223.139
  - resource_id: 15.236.223.139
  - allocation_id: eipalloc-0134ceb9112c887fd
  - domain: vpc
  - network_border_group: eu-west-3
  - public_ipv4_pool: amazon
  - tags:
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-natgw-eip-0
    - Key: Owner
      Value: org1
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Environment
      Value: test-dev
