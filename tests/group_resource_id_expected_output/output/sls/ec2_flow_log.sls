fl-02e639031d2f37592:
  aws.ec2.flow_log.present:
  - resource_ids:
    - vpc-02e366ce9fa15dd56
  - resource_type: VPC
  - resource_id: fl-02e639031d2f37592
  - iam_role: arn:aws:iam::123456789012:role/xyz-idem-test_redlock_flow_role
  - log_group_name: xyz-idem-test_redlock_flow_log_group
  - traffic_type: ALL
  - log_destination_type: cloud-watch-logs
  - log_destination: arn:aws:logs:eu-west-3:123456789012:log-group:xyz-idem-test_redlock_flow_log_group:*
  - log_format: ${version} ${account-id} ${interface-id} ${srcaddr} ${dstaddr} ${srcport}
      ${dstport} ${protocol} ${packets} ${bytes} ${start} ${end} ${action} ${log-status}
  - max_aggregation_interval: 600
  - tags: []


fl-0604c326e4d398406:
  aws.ec2.flow_log.present:
  - resource_ids:
    - vpc-0c06512aed6737787
  - resource_type: VPC
  - resource_id: fl-0604c326e4d398406
  - iam_role: arn:aws:iam::123456789012:role/xyz-idem-test_redlock_flow_role
  - log_group_name: xyz-idem-test_redlock_flow_log_group
  - traffic_type: ALL
  - log_destination_type: cloud-watch-logs
  - log_destination: arn:aws:logs:eu-west-3:123456789012:log-group:xyz-idem-test_redlock_flow_log_group:*
  - log_format: ${version} ${account-id} ${interface-id} ${srcaddr} ${dstaddr} ${srcport}
      ${dstport} ${protocol} ${packets} ${bytes} ${start} ${end} ${action} ${log-status}
  - max_aggregation_interval: 600
  - tags: []


fl-0761ee74ec872ab13:
  aws.ec2.flow_log.present:
  - resource_ids:
    - vpc-079e00e8877b676f8
  - resource_type: VPC
  - resource_id: fl-0761ee74ec872ab13
  - iam_role: arn:aws:iam::123456789012:role/xyz-idem-test_redlock_flow_role
  - log_group_name: xyz-idem-test_redlock_flow_log_group
  - traffic_type: ALL
  - log_destination_type: cloud-watch-logs
  - log_destination: arn:aws:logs:eu-west-3:123456789012:log-group:xyz-idem-test_redlock_flow_log_group:*
  - log_format: ${version} ${account-id} ${interface-id} ${srcaddr} ${dstaddr} ${srcport}
      ${dstport} ${protocol} ${packets} ${bytes} ${start} ${end} ${action} ${log-status}
  - max_aggregation_interval: 600
  - tags: []


fl-0d8347dbe88a0027a:
  aws.ec2.flow_log.present:
  - resource_ids:
    - vpc-0738f2a523f4735bd
  - resource_type: VPC
  - resource_id: fl-0d8347dbe88a0027a
  - iam_role: arn:aws:iam::123456789012:role/xyz-idem-test_redlock_flow_role
  - log_group_name: xyz-idem-test_redlock_flow_log_group
  - traffic_type: ALL
  - log_destination_type: cloud-watch-logs
  - log_destination: arn:aws:logs:eu-west-3:123456789012:log-group:xyz-idem-test_redlock_flow_log_group
  - log_format: ${version} ${account-id} ${interface-id} ${srcaddr} ${dstaddr} ${srcport}
      ${dstport} ${protocol} ${packets} ${bytes} ${start} ${end} ${action} ${log-status}
  - max_aggregation_interval: 600
  - tags: []
