nat-076cd14a28acd21b4:
  aws.ec2.nat_gateway.present:
  - name: nat-076cd14a28acd21b4
  - resource_id: nat-076cd14a28acd21b4
  - subnet_id: subnet-0d68d61b1ab708d42
  - connectivity_type: public
  - tags:
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-natgw-2
  - state: available
  - allocation_id: eipalloc-01319ee06efe14298


nat-0a49a65a4bb87370a:
  aws.ec2.nat_gateway.present:
  - name: nat-0a49a65a4bb87370a
  - resource_id: nat-0a49a65a4bb87370a
  - subnet_id: subnet-09cecc8c853637d3b
  - connectivity_type: public
  - tags:
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-natgw-0
  - state: available
  - allocation_id: eipalloc-0134ceb9112c887fd


nat-0c02a1f1d590b5534:
  aws.ec2.nat_gateway.present:
  - name: nat-0c02a1f1d590b5534
  - resource_id: nat-0c02a1f1d590b5534
  - subnet_id: subnet-0094b72dfb7ce6131
  - connectivity_type: public
  - tags:
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-natgw-1
  - state: available
  - allocation_id: eipalloc-001d4219447c325ca
