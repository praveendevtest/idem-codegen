## IAM admin role for creating the cluster
resource "aws_iam_role" "xyz-admin" {
  name               = "xyz-${var.clusterName}-admin"
  tags               = local.tags
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "${data.aws_caller_identity.current.account_id}"
      },
      "Action": "sts:AssumeRole",
      "Condition": {
        "ForAnyValue:StringEquals": {
          "aws:username": ${jsonencode(var.admin_users)}
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "xyz.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF

}

#          "aws:username": ${jsonencode(distinct(flatten(list(var.default_admins, var.admin_users))))}

output "aws_iam_role-xyz-admin" {
  value = aws_iam_role.xyz-admin.arn
}

## IAM role policy for xyz-admin
resource "aws_iam_policy" "xyz-admin" {
  name        = "xyz-${var.clusterName}-admin"
  description = "iam policy for xyz-admin"
  policy      = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
        "Action": [
            "xyz:*"
        ],
        "Effect": "Allow",
        "Resource": "*"
    },
    {
        "Action": [
            "iam:PassRole"
        ],
        "Effect": "Allow",
        "Resource": [
          "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/${var.clusterName}-temp-xyz-cluster"
        ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "kms:Create*",
        "kms:Describe*",
        "kms:Enable*",
        "kms:List*",
        "kms:Put*",
        "kms:Update*",
        "kms:Revoke*",
        "kms:Disable*",
        "kms:Get*",
        "kms:Delete*",
        "kms:TagResource",
        "kms:UntagResource",
        "kms:ScheduleKeyDeletion",
        "kms:CancelKeyDeletion"
      ],
      "Resource": "${aws_kms_key.credstash_key.arn}"
    }
  ]
}
EOF

}

## iam role policy attachment for xyz-admin
resource "aws_iam_role_policy_attachment" "xyz-admin" {
  role       = aws_iam_role.xyz-admin.name
  policy_arn = aws_iam_policy.xyz-admin.arn
}

# resource "aws_iam_role_policy_attachment" "cluster-AmazonxyzClusterPolicy" {
#   policy_arn = "arn:aws:iam::aws:policy/AmazonxyzClusterPolicy"
#   role       = "${aws_iam_role.xyz-admin.name}"
# }
#
# resource "aws_iam_role_policy_attachment" "cluster-AmazonxyzServicePolicy" {
#   policy_arn = "arn:aws:iam::aws:policy/AmazonxyzServicePolicy"
#   role       = "${aws_iam_role.xyz-admin.name}"
# }
