resource "aws_iam_role" "cluster" {
  name = "${var.clusterName}-temp-xyz-cluster"
  tags = local.tags
  test_output_variable = var.aws_kms_key-credstash_key-key-id

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "xyz.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY

}

resource "aws_iam_role_policy_attachment" "cluster-AmazonxyzClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonxyzClusterPolicy"
  role       = aws_iam_role.cluster.name
}

resource "aws_iam_role_policy_attachment" "cluster-AmazonxyzServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonxyzServicePolicy"
  role       = aws_iam_role.cluster.name
}
