# ToDo: The attribute zone_id has resolved value of data state. Please create a variable with resolved value and use { params.get(variable_name) } instead of resolved value of data state.
# ToDo: The attribute vpc_id has resolved value of data state. Please create a variable with resolved value and use { params.get(variable_name) } instead of resolved value of data state.
aws_route53_zone_association.internal-potato-beachops-io:
  aws.route53.hosted_zone_association.present:
  - resource_id: {{ params.get("aws_route53_zone_association.internal-potato-beachops-io")}}
  - name: Z0721725PI001CQXUGYY:vpc-0738f2a523f4735bd:eu-west-3
  - zone_id: Z0721725PI001CQXUGYY
  - vpc_id: {{ params.get("create_vpc") }} ? ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id}
      : data.aws_vpc.vpc[0].id
  - vpc_region: {{params.get("region")}}
  - comment:

# ToDo: The attribute zone_id has resolved value of data state. Please create a variable with resolved value and use { params.get(variable_name) } instead of resolved value of data state.
# ToDo: The attribute vpc_id has resolved value of data state. Please create a variable with resolved value and use { params.get(variable_name) } instead of resolved value of data state.
aws_route53_zone_association.msk_kafka_domain:
  aws.route53.hosted_zone_association.present:
  - resource_id: {{ params.get("aws_route53_zone_association.msk_kafka_domain")}}
  - name: Z03616682JNY9P3D3T2IR:vpc-0738f2a523f4735bd:eu-west-3
  - zone_id: Z03616682JNY9P3D3T2IR
  - vpc_id: {{ params.get("create_vpc") }} ? ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id}
      : data.aws_vpc.vpc[0].id
  - vpc_region: {{params.get("region")}}
  - comment:

data.aws_route53_zone.cross-account-internal-potato-beachops-io:
  aws.route53.hosted_zone.search:
  - filters:
    - name: provider
      value: ${aws.cross_account_beachops}
    - name: name
      value: "internal.${replace(\n    var.cross_account_beachops_domain_profile,\n\
        \    \"/sym-.*/\",\n    \"potato\",\n  )}.beachops.io."
    - name: private_zone
      value: true

data.aws_route53_zone.internal-potato-beachops-io:
  aws.route53.hosted_zone.search:
  - filters:
    - name: name
      value: internal.${replace(var.profile, "/sym-.*/", "potato")}.beachops.io.
    - name: private_zone
      value: true

data.aws_route53_zone.msk_kafka_domain:
  aws.route53.hosted_zone.search:
  - filters:
    - name: name
      value: kafka.{{ params.get("region") }}.amazonaws.com
    - name: private_zone
      value: true
