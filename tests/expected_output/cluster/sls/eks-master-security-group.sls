# ToDo: The attribute vpc_id has resolved value of data state. Please create a variable with resolved value and use { params.get(variable_name) } instead of resolved value of data state.
aws_security_group.cluster:
  aws.ec2.security_group.present:
  - resource_id: {{ params.get("aws_security_group.cluster")}}
  - name: {{ params.get("clusterName") }}-temp-xyz-cluster
  - vpc_id: {{ params.get("create_vpc") }} ? ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id}
      : data.aws_vpc.vpc[0].id
  - tags: {{ params.get("local_tags") + [{"Key": "Name", "Value": params.get("clusterName")+"-temp-xyz"}]}}
  - description: Cluster communication with worker nodes

aws_security_group.cluster-rule-0:
  aws.ec2.security_group_rule.present:
  - name: sgr-077c2651cc2eb9b1f
  - resource_id: {{ params.get("aws_security_group.cluster-rule-0")}}
  - group_id: ${aws.ec2.security_group:aws_security_group.cluster:resource_id}
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 443
  - to_port: 443
  - tags: []
  - description: Allow pods to communicate with the cluster API Server
  - referenced_group_info:
      GroupId: ${aws.ec2.security_group:aws_security_group.cluster-node:resource_id}
      UserId: 123456789012

aws_security_group.cluster-rule-1:
  aws.ec2.security_group_rule.present:
  - name: sgr-0a4ef3a78cdce5042
  - resource_id: {{ params.get("aws_security_group.cluster-rule-1")}}
  - group_id: ${aws.ec2.security_group:aws_security_group.cluster:resource_id}
  - is_egress: true
  - ip_protocol: -1
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []
